//
//  VenueMeViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 13/04/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class VenueMeViewController: UIViewController {
    
    // MARK: - Model
    
    
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }    
}

// MARK: - Status Bar

extension VenueMeViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


