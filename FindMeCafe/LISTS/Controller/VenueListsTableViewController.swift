//
//  VenueListsTableViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 12/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class VenueListsTableViewController: UITableViewController {

    // MARK: - Model
    
    // MARK: - Properties
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
        }) { finished in
        }
        
        // needs some tuning, so each scroll will end up with the "section title" at the top
//        guard let visibleRows = tableView.indexPathsForVisibleRows else { return }
//        coordinator.animate(alongsideTransition: { _ in
//            self.tableView.reloadData()
//            self.tableView.scrollToRow(at: visibleRows[0], at: .top, animated: true)
//        })
    }
}

// MARK: - Status Bar

extension VenueListsTableViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - UITableViewDelegate

extension VenueListsTableViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.orientation.isLandscape {
            let barsHeight = (navigationController?.navigationBar.bounds.height ?? 0) + (tabBarController?.tabBar.bounds.height ?? 0)
            
            switch indexPath.row {
            // featured spots, saved places and lists
            case 1, 3, 5: return tableView.bounds.height - barsHeight
            default: return UITableView.automaticDimension
            }
        } else {
            switch indexPath.row {
            // featured spots
            case 1: return tableView.bounds.width * 5 / 8 + 8
            // saved places and lists
            case 3, 5: return tableView.bounds.width / 2
            default: return UITableView.automaticDimension
            }
        }
    }
}



