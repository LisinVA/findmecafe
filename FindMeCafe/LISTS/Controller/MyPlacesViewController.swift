//
//  MyPlacesViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 12/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class MyPlacesViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        collectionView.collectionViewLayout.invalidateLayout()
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MyPlacesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (collectionView.bounds.width - 8 * 3) / 2
        let height = collectionView.bounds.height - 2
        return CGSize(width: width, height: height)
    }
}

// MARK: - UICollectionViewDataSource

extension MyPlacesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // TODO: for each cell implement collection view
        // (instead of static views/images)
        
        let cellID: String
        switch indexPath.item {
        case 0: cellID = "Saved Places Cell"
        default: cellID = "Liked Places Cell"
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        return cell
    }
}
