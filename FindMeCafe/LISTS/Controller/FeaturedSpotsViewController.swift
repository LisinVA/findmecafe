//
//  FeaturedSpotsViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 12/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class FeaturedSpotsViewController: UIViewController {

    // MARK: - Model

    var spots = [FeaturedSpot]()
    
    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: - Properties
    
    private var viewWillRotate = false
    private var indexPathBeforeRotation = IndexPath(item: 0, section: 0)
    
    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        setupLayout()
        generateModel()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.collectionViewLayout.invalidateLayout()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if viewWillRotate {
            collectionView.scrollToItem(at: indexPathBeforeRotation, at: .centeredHorizontally, animated: false)
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        viewWillRotate = true
        indexPathBeforeRotation = IndexPath(item: pageControl.currentPage, section: 0)
        coordinator.animate(alongsideTransition: { _ in
        }) { finished in
            self.viewWillRotate = false
        }
    }
    
    // MARK: - Collection View Setup
    
    private func setupCollectionView() {
        collectionView.isPagingEnabled = false
        collectionView.decelerationRate = .fast
    }
    
    private func setupLayout() {
        guard let layout = collectionView.collectionViewLayout as? SingleItemPagingHorizontalFlowLayout else {
            return
        }
        layout.minimumLineSpacing = 4
        layout.sectionInset = UIEdgeInsets(
            top: 0, left: 2 * layout.minimumLineSpacing,
            bottom: 0, right: 2 * layout.minimumLineSpacing
        )
        layout.scrollDirection = .horizontal
    }
    
    // MARK: - Model Setup
    
    private func generateModel() {
        spots = [
            FeaturedSpot(
                photo: UIImage(named: "FeaturedSpotsIntro"),
                ownerIcon: UIImage(named: "Foursquare-logo_red"),
                title: "Go Ahead, Be A Tourist",
                text: "We've collected some of the top spots that travelers like to visit around the world. Use Swarm to check in at one of these places and you'll unlock The Tourist sticker!"
            ),
            FeaturedSpot(
                photo: UIImage(named: "FreeWiFiAirports"),
                ownerIcon: UIImage(named: "FreeWiFiAirports_owner"),
                title: "Free WiFi Airports",
                text: "Airports providing free WiFi internet. Part 1"
            ),
            FeaturedSpot(
                photo: UIImage(named: "TopDinnerSpots"),
                ownerIcon: UIImage(named: "TopDinnerSpots_owner"),
                title: "Top dinner spots in Akademgorodok",
                text: "by Sergey Zhatchenko"
            ),
            FeaturedSpot(
                photo: UIImage(named: "AirportsAroundTheWorld"),
                ownerIcon: UIImage(named: "AirportsAroundTheWorld_owner"),
                title: "Airports (around the world)",
                text: "Airports with ..."
            ),
            FeaturedSpot(
                photo: UIImage(named: "Novosibirsk"),
                ownerIcon: UIImage(named: "Novosibirsk_owner"),
                title: "Novosibirsk",
                text: "by Anton Williams"
            )
        ]
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension FeaturedSpotsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let flowLayout = collectionViewLayout as? SingleItemPagingHorizontalFlowLayout else {
            return CGSize.zero
        }
        let width = collectionView.bounds.width - 4 * flowLayout.minimumLineSpacing
        let height = collectionView.bounds.height
        flowLayout.itemSize = CGSize(width: width, height: height)
        return CGSize(width: width, height: height)
    }
}

// MARK: - UICollectionViewDataSource

extension FeaturedSpotsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Featured Spot Cell", for: indexPath)
        
        if let cell = cell as? FeaturedSpotCollectionViewCell {
            let index = indexPath.item
            cell.spot = spots[index]
            if index == 0 || index == spots.count - 1 {
                cell.topCornerView.isHidden = true
            } else {
                cell.topCornerView.isHidden = false
            }
        }
        return cell
    }
}

// MARK: - UIScrollViewDelegate

extension FeaturedSpotsViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = view.convert(collectionView.center, to: collectionView)
        if let currentIndexPath = collectionView.indexPathForItem(at: center) {
            pageControl.currentPage = currentIndexPath.item
        }
    }
}
