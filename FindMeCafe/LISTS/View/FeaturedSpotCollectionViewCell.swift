//
//  FeaturedSpotCollectionViewCell.swift
//  FindMeCafe
//
//  Created by zweqi on 21/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class FeaturedSpotCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Model
    
    var spot: FeaturedSpot! { didSet { updateUI() } }
    
    // MARK: - Storyboard

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ownerIconImageView: UIImageView!
    @IBOutlet weak var topCornerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
}

// MARK: - Updating UI

extension FeaturedSpotCollectionViewCell {
    
    private func updateUI() {
        photoImageView.image = spot.photo
        ownerIconImageView.image = spot.ownerIcon
        titleLabel.text = spot.title
        textLabel.text = spot.text
    }
}
