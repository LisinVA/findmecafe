//
//  FeaturedSpot.swift
//  FindMeCafe
//
//  Created by zweqi on 21/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

struct FeaturedSpot {
    
    let photo: UIImage?
    let ownerIcon: UIImage?
    let title: String?
    let text: String?
}
