//
//  VenueFeatures.swift
//  FindMeCafe
//
//  Created by zweqi on 30/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

// TODO: rename and move to CoreData class
struct  VenueFeatures {
    
    let isCheap: Bool
    let isTrendy: Bool
    let isKidFriendly: Bool
    let isGoodForAQuickBite: Bool
    let isGreatForGroups: Bool
    
    init(isCheap: Bool, isTrendy: Bool, isKidFriendly: Bool = true, isGoodForAQuickBite: Bool = true, isGreatForGroups: Bool = true) {
        
        self.isCheap = isCheap
        self.isTrendy = isTrendy
        self.isKidFriendly = isKidFriendly
        self.isGoodForAQuickBite = isGoodForAQuickBite
        self.isGreatForGroups = isGreatForGroups
    }
}
