//
//  VenueRating.swift
//  FindMeCafe
//
//  Created by zweqi on 28/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

// TODO: rename and move to CoreData class
// (after implementing Foursquare API fetch of the real contents)
struct VenueRating {
    
    let rating: CGFloat
    let visitors: Int
    let likes: Int
    let oks: Int
    let dislikes: Int
    
    let tintColor: UIColor

    private var votesTotal: Int {
        return likes + oks + dislikes
    }
    
    var likeRatio: CGFloat {
        if isPopulated {
            let ratio = CGFloat(likes) / CGFloat(votesTotal)
            return (ratio * 100).rounded() / 100
        } else {
            return 0
        }
    }
    var okRatio: CGFloat {
        if isPopulated {
            let ratio = CGFloat(oks) / CGFloat(votesTotal)
            return (ratio * 100).rounded() / 100
        } else {
            return 0
        }
    }
    var dislikeRatio: CGFloat {
        return isPopulated ? 1 - likeRatio - okRatio : 0
    }

    var isPopulated: Bool {
        return rating != 0 && visitors != 0 && (likes != 0 || oks != 0 || dislikes != 0)
    }
    
    init?(from venue: Venue?) {
        guard let venue = venue else { return nil }
        
        self.rating = CGFloat(venue.rating?.value ?? 0)
        self.visitors = Int(venue.votesCount)
        self.likes = Int(venue.likes)
        self.oks = Int.random(in: 0...2 * self.likes)   // TODO: get the actual data
        self.dislikes = Int.random(in: 0...self.likes)  // TODO: get the actual data
        self.tintColor = venue.rating?.colorHex?.uicolor ?? ColorPalette.highlightsGreen
    }
}

// MARK: - CustomStringConvertible

extension VenueRating: CustomStringConvertible {
    
    var description: String {
        return String("""
            rating - \(rating),
            visitors -  \(visitors),
            likes - \(likes),
            oks - \(oks),
            dislikes - \(dislikes)
        """)
    }
}
