//
//  MenuBarCell.swift
//  FindMeCafe
//
//  Created by zweqi on 19/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class MenuBarCell: UICollectionViewCell {
    
    private enum CellState {
        case normal, highlighted, selected
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    private let fontSize: CGFloat = 13
    var selectedTextColor = UIColor.white {
        didSet { updateUI() }
    }
    var regularTextColor: UIColor {
        if selectedTextColor.isDark {
            return ColorPalette.menuBarDarkText
        } else {
            return ColorPalette.menuBarLightText
        }
    }
    
    private var state: CellState = .normal {
        didSet { updateUI() }
    }
    
    override var isHighlighted: Bool {
        didSet { state = isHighlighted ? .highlighted : .normal }
    }
    
    override var isSelected: Bool {
        didSet { state = isSelected ? .selected : .normal }
    }
    
    private func updateUI() {
        switch state {
        case .normal:
            titleLabel.textColor = regularTextColor
            titleLabel.font = UIFont.systemFont(ofSize: fontSize)
        case .highlighted, .selected:
            titleLabel.textColor = selectedTextColor
            titleLabel.font = UIFont.boldSystemFont(ofSize: fontSize)
        }
    }
}
