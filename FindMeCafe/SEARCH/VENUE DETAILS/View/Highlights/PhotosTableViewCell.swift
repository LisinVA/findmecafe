//
//  PhotosTableViewCell.swift
//  FindMeCafe
//
//  Created by zweqi on 22/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class PhotosTableViewCell: UITableViewCell {

//    private var photos: [Photo]
    
    @IBOutlet weak var topLeftPhoto: RoundedImageView!
    @IBOutlet weak var topRightPhoto: RoundedImageView!
    @IBOutlet weak var bottomLeftPhoto: RoundedImageView!
    @IBOutlet weak var bottomRightPhoto: RoundedImageView!
    
    @IBOutlet weak var topLeftPhotoOverlay: UIView!
    @IBOutlet weak var topRightPhotoOverlay: UIView!
    @IBOutlet weak var bottomLeftPhotoOverlay: UIView!
    @IBOutlet weak var bottomRightPhotoOverlay: UIView!
    
    @IBOutlet weak var addPhotosStackView: UIStackView!
    
    
    // fetch photos
    // update UI
}
