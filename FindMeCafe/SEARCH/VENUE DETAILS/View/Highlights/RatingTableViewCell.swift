//
//  RatingTableViewCell.swift
//  FindMeCafe
//
//  Created by zweqi on 22/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class RatingTableViewCell: UITableViewCell {

    var venueRating: VenueRating? {
        didSet { updateUI() }
    }
    
    // labels
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var oksLabel: UILabel!
    @IBOutlet weak var dislikesLabel: UILabel!
    
    // views
    @IBOutlet weak var circleView: AnimatedCircleView!
    @IBOutlet weak var circleLockImageView: UIImageView!
    
    @IBOutlet weak var likesImageView: UIImageView!
    @IBOutlet weak var oksImageView: UIImageView!
    @IBOutlet weak var dislikesImageView: UIImageView!
    
    @IBOutlet weak var likesLineView: AnimatedLineView!
    @IBOutlet weak var oksLineView: AnimatedLineView!
    @IBOutlet weak var dislikesLineView: AnimatedLineView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureAnimation()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        startAnimation()
    }
}

// MARK: - Update UI

extension RatingTableViewCell {

    private func updateUI() {
        guard venueRating?.isPopulated == true else { return }

        updateLabels()
        updateImages()
        updateShapes()
    }
    
    private func updateLabels() {
        guard let rating = venueRating else { return }

        ratingLabel.isHidden = false
        ratingLabel.textColor = rating.tintColor
        
        let textColor = ColorPalette.highlightsTextDark
        likesLabel.textColor = textColor
        oksLabel.textColor = textColor
        dislikesLabel.textColor = textColor
        
        let visitorSuffix = rating.visitors % 10 != 1 ? "s" : ""
        titleLabel.text = "Rated \(rating.rating) out of 10 based on \(rating.visitors) visitor" + visitorSuffix
        ratingLabel.text = rating.rating.decimalString
        likesLabel.text = rating.likeRatio.percentString + " Likes"
        oksLabel.text = rating.okRatio.percentString + " It's OK"
        dislikesLabel.text = rating.dislikeRatio.percentString + " Dislikes"
    }
    
    private func updateImages() {
        guard let details = venueRating else { return }
        
        circleLockImageView.isHidden = true
        
        likesImageView.image = UIImage(named: "full_heart")
        oksImageView.image = UIImage(named: "face")
        dislikesImageView.image = UIImage(named: "broken_heart")
        
        likesImageView.tintColor = details.tintColor
        oksImageView.tintColor = details.tintColor
        dislikesImageView.tintColor = details.tintColor
    }
    
    private func updateShapes() {
        guard let rating = venueRating else { return }

        circleView.animationStrokeColor = rating.tintColor
        likesLineView.animationStrokeColor = rating.tintColor
        oksLineView.animationStrokeColor = rating.tintColor
        dislikesLineView.animationStrokeColor = rating.tintColor
    }
}

// MARK: - Animation

extension RatingTableViewCell {
    
    private func configureAnimation() {
        let lineWidth = likesLineView.lineWidth
        circleView.lineWidth = lineWidth
        
        let animationDuration: CFTimeInterval = 2
        circleView.animationDuration = animationDuration
        likesLineView.animationDuration = animationDuration
        oksLineView.animationDuration = animationDuration
        dislikesLineView.animationDuration = animationDuration
    }
    
    func startAnimation() {
        guard let rating = venueRating, rating.isPopulated else { return }
        
        circleView.animate(progress: rating.rating / 10)
        likesLineView.animate(progress: rating.likeRatio)
        oksLineView.animate(progress: rating.okRatio)
        dislikesLineView.animate(progress: rating.dislikeRatio)
    }
}
