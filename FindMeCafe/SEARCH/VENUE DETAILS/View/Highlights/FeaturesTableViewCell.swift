//
//  FeaturesTableViewCell.swift
//  FindMeCafe
//
//  Created by zweqi on 22/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class FeaturesTableViewCell: UITableViewCell {

    var venueFeatures: VenueFeatures? {
        didSet { updateFeatures() }
    }
    
    @IBOutlet weak var cheapCheckMark: UIImageView!
    @IBOutlet weak var trendyCheckMark: UIImageView!
    @IBOutlet weak var kidFriendlyCheckMark: UIImageView!
    @IBOutlet weak var quickBiteCheckMark: UIImageView!
    @IBOutlet weak var greatForGroupsCheckMark: UIImageView!
    
    @IBOutlet weak var cheapLabel: UILabel!
    @IBOutlet weak var trendyLabel: UILabel!
    @IBOutlet weak var kidFriendlyLabel: UILabel!
    @IBOutlet weak var quickBiteLabel: UILabel!
    @IBOutlet weak var greatForGroupsLabel: UILabel!
}

// MARK: - Update UI

extension FeaturesTableViewCell {
    
    private func updateFeatures() {
        guard let features = venueFeatures else { return }
        
        cheapCheckMark.isHidden = !features.isCheap
        cheapLabel.isHidden = !features.isCheap
        trendyCheckMark.isHidden = !features.isTrendy
        trendyLabel.isHidden = !features.isTrendy
        kidFriendlyCheckMark.isHidden = !features.isKidFriendly
        kidFriendlyLabel.isHidden = !features.isKidFriendly
        quickBiteCheckMark.isHidden = !features.isGoodForAQuickBite
        quickBiteLabel.isHidden = !features.isGoodForAQuickBite
        greatForGroupsCheckMark.isHidden = !features.isGreatForGroups
        greatForGroupsLabel.isHidden = !features.isGreatForGroups
    }
}
