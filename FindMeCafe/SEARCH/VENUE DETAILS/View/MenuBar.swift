//
//  MenuBarCV.swift
//  FindMeCafe
//
//  Created by zweqi on 29/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

protocol MenuBarDelegate: class {
    
    func didSelectItem(at index: Int)
}

// TODO: wrap the whole class in UIViewController
class MenuBar: UIView {
    
    // MARK: - Model
    
    let cellTitles = ["Highlights", "Info", "Photos", "Tips"]
    
    // MARK: - Properties

    weak var delegate: MenuBarDelegate?

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    lazy var slider: UIView = {
        let slider = UIView()
        slider.translatesAutoresizingMaskIntoConstraints = false
        return slider
    }()
    private var sliderLeftAnchor:  NSLayoutConstraint?
    private var sliderRightAnchor:  NSLayoutConstraint?
    
    var numberOfCells: Int { return cellTitles.count }
    var cellWidth: CGFloat { return bounds.width / CGFloat(numberOfCells) }
    var textColor = UIColor.white { didSet { updateColors() } }
    
    // MARK: - Methods
    
    func selectItem(at index: Int) {
        collectionView.selectItem(at: IndexPath(item: index, section: 0), animated: true, scrollPosition: .left)
    }
    
    func moveSlider(by offset: CGFloat) {
        sliderLeftAnchor?.constant = offset
    }
    
    private func updateColors() {
        let indexPathForSelectedItem = collectionView.indexPathsForSelectedItems?.first
        slider.backgroundColor = textColor.withAlphaComponent(0.8)
        collectionView.reloadData()
        if let indexPath = indexPathForSelectedItem {
            selectItem(at: indexPath.item)
        }
    }
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupSlider()
        selectItem(at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func setupSlider() {
        addSubview(slider)
        slider.heightAnchor.constraint(equalToConstant: 2).isActive = true
        slider.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        updateSliderSideConstraints(
            left: slider.leftAnchor.constraint(equalTo: leftAnchor),
            right: slider.rightAnchor.constraint(equalTo: leftAnchor, constant: cellWidth),
            animate: false
        )
    }
    
    private func updateSliderSideConstraints(left leftAnchor: NSLayoutConstraint, right rightAnchor: NSLayoutConstraint, animate: Bool = true) {
        sliderLeftAnchor?.isActive = false
        sliderRightAnchor?.isActive = false
        sliderLeftAnchor = leftAnchor
        sliderRightAnchor = rightAnchor
        sliderLeftAnchor?.isActive = true
        sliderRightAnchor?.isActive = true
        
        if animate {
            UIView.animate(withDuration: 0.3, animations: { self.layoutIfNeeded() })
        }
    }
}

// MARK: - UICollectionViewDelegate

extension MenuBar: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) else { return }

        updateSliderSideConstraints(
            left: slider.leftAnchor.constraint(equalTo: cell.leftAnchor),
            right: slider.rightAnchor.constraint(equalTo: cell.rightAnchor)
        )
        delegate?.didSelectItem(at: indexPath.item)
    }
}

// MARK: - UICollectionViewDataSource

extension MenuBar: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfCells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuBarCell", for: indexPath)
        
        if let cell = cell as? MenuBarCell {
            cell.titleLabel.text = cellTitles[indexPath.item]
            cell.selectedTextColor = textColor
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MenuBar: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: bounds.height)
    }
}
