//
//  PlusMenuViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 07/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class PlusMenuViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet var buttonsView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet var stackViews: [UIStackView]!
    
    // plus button
    @IBOutlet weak var plusButton: RoundButton!
    @IBAction func plusTouched(_ sender: RoundButton) {
        hideMenu()
    }
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonsView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(hideMenu))
        )
        blurView.effect = nil
        prepareButtonsForAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showMenu()
    }
}

// MARK: - Animation

extension PlusMenuViewController {
    
    private func prepareButtonsForAnimation() {
        func moveViewToPlusButton(_ view: UIView) {
            let yInTopParentView = self.view.convert(view.frame, from: view.superview).origin.y
            
            view.transform = CGAffineTransform(translationX: 0, y: plusButton.frame.origin.y - yInTopParentView)
        }
        stackViews.forEach {
            $0.alpha = 0.3
            moveViewToPlusButton($0)
        }
    }

    private func showMenu() {
        showBackground()
        showMenuButtons()
        animatePlusButton()
    }
    
    @objc private func hideMenu() {
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.blurView.effect = nil
                self.mainStackView.alpha = 0
            }
        ) { finished in
            self.presentingViewController?.dismiss(animated: false)
        }
        animatePlusButton()
    }
    
    private func showBackground() {
        UIView.animate(withDuration: 0.7) {
            self.blurView.effect = UIBlurEffect(style: .light)
        }
    }

    private func showMenuButtons() {
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 0.8,
            initialSpringVelocity: 1,
            options: .curveEaseOut,
            animations: {
                self.stackViews.forEach {
                    $0.alpha = 1
                    $0.transform = .identity
                }
            }
        )
    }
    
    private func animatePlusButton() {
        UIView.animate(withDuration: 0.3) {
            if self.plusButton.transform == .identity {
                self.plusButton.transform = CGAffineTransform(rotationAngle: .pi/4)
            } else {
                self.plusButton.transform = .identity
            }
        }
    }
}
