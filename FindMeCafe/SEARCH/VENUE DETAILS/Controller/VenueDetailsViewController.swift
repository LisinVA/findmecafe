//
//  VenueDetailsViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 04/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

class VenueDetailsViewController: UIViewController {
    
    // MARK: - Model
    
    var container: NSPersistentContainer? = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
    
    private var venue: Venue? {
        if let context = container?.viewContext {
            return Venue.selectedVenue(context: context)
        } else {
            return nil
        }
    }
    var venuePreviewImage: UIImage?
    
    private var receiptID = "Venue Details"
    
    // MARK: - Outlets

    // Top View Image
    // TODO: put in a custom class
    @IBOutlet weak var topImageBackground: UIView!
    @IBOutlet weak var topImageGradientContainer: GradientView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var topImageBlurOverlay: UIView!
    @IBOutlet weak var topImageViewHeight: NSLayoutConstraint!
    var gradientContainerHeight: NSLayoutConstraint?
    
    // Top View Labels
    // TODO: put in a custom class
    @IBOutlet weak var venueInfoStackView: UIStackView!
    @IBOutlet weak var venueNameLabel: UILabel!
    @IBOutlet weak var venueSubtitleLabel: UILabel!
    @IBOutlet weak var subtitleDelimeterLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    // Bars, Buttons
    @IBOutlet weak var navBar: BaseNavigationBar!
    @IBAction func pop(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func moreBarButton(_ sender: UIBarButtonItem) {
        showMoreActionSheet()
    }
    @IBOutlet weak var menuBar: MenuBar!
    @IBOutlet weak var plusButton: RoundButton!
    
    // MARK: - Properties
    
    private var pageVC: VenueDetailsPageViewController?
    var topViewDefaultHeight: CGFloat {
        return view.bounds.height / 2
    }
    var topViewMinHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.height +
            (navigationController?.navigationBar.bounds.height ?? 0) +
            menuBar.bounds.height
    }
    private var mainBackgroundColor = UIColor.lightGray
    private var mainTextColor = UIColor.white {
        didSet { setNeedsStatusBarAppearanceUpdate() }
    }
    
    // MARK: - Services
    
    private var queryService: FoursquareAPIClient {
        return FoursquareAPIClient.shared
    }
    private var locationManager: LocationManager {
        return LocationManager.shared
    }

    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        setupTopView()
        setupMainColors()
        updateVenueDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationManager.locationQuality = .low
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in
            self.updateTopViewConstraints()
        })
    }
    
    deinit {
        Venue.selectedVenueID = nil
    }
}

// MARK: - Navigation

extension VenueDetailsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Embed Pages" {
            pageVC = segue.destination as? VenueDetailsPageViewController
            pageVC?.mainVC = self
            menuBar.delegate = pageVC
        }
    }
}
    
// MARK: - Status Bar

extension VenueDetailsViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return mainTextColor.isDark ? .default : .lightContent
    }
}

// MARK: - Navigation Bar

extension VenueDetailsViewController {

    private func setupNavBar() {
        navBar.setBackgroundColor(.clear)
        navBar.topItem?.title = venue?.name
        navBar.delegate = self
    }
    
    private func updateNavBar(_ shownProgress: CGFloat) {
        if shownProgress > 0 {
            navBar.setBackgroundColor(.clear)
            navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: mainTextColor.withAlphaComponent(1 - 2 * shownProgress)]
        } else {
            navBar.setBackgroundColor(mainBackgroundColor)
        }
    }
}

// MARK: - UINavigationBarDelegate

extension VenueDetailsViewController: UINavigationBarDelegate {
    
    public func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

// MARK: - Dominant Colors

extension VenueDetailsViewController {

    private func setupMainColors() {
        guard let imageColors = topImageView.image?.getColors(quality: .lowest) else { return }
        
        mainBackgroundColor = imageColors.background
        mainTextColor = imageColors.text
        
        topImageBackground.backgroundColor = mainBackgroundColor
        navBar.tintColor = mainTextColor
        navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.clear]
        
        menuBar.backgroundColor = mainBackgroundColor
        menuBar.textColor = mainTextColor
        
        venueNameLabel.textColor = mainTextColor
        venueSubtitleLabel.textColor = mainTextColor
        subtitleDelimeterLabel.textColor = mainTextColor
        let style: FoursquareAPI.Price.Style = mainTextColor.isDark ? .dark : .light
        priceLabel.attributedText = venue?.price?.attributed(style: style)
    }
}

// MARK: - Top View

extension VenueDetailsViewController {

    private func setupTopView() {
        updateVenueImage()
        updateTopViewConstraints()
        setupTopViewLabels()
        setupGradient()
    }
    
    private func updateVenueImage() {
        topImageView.image = venuePreviewImage
        guard let url = venue?.featuredPhoto?.url(size: .size500x500) else { return }

        queryService.fetchVenueImage(from: url, receiptID: receiptID) { [weak self] result in
            if case .success(let photo) = result {
                guard url == self?.venue?.featuredPhoto?.url(size: .size500x500) else { return }
                self?.topImageView.image = photo
                self?.topImageBlurOverlay.isHidden = true
            }
        }
    }
    
    private func updateTopViewConstraints() {
        if gradientContainerHeight == nil {
            gradientContainerHeight = topImageGradientContainer.heightAnchor.constraint(equalToConstant: topViewDefaultHeight)
            gradientContainerHeight?.isActive = true
        }
        gradientContainerHeight?.constant = topViewDefaultHeight
        topImageViewHeight.constant = topViewDefaultHeight
    }
    
    private func setupTopViewLabels() {
        venueNameLabel.text = venue?.name
        venueSubtitleLabel.text = venue?.category
        priceLabel.attributedText = venue?.price?.attributed(style: .light)
        subtitleDelimeterLabel.isHidden = (priceLabel.attributedText == nil)
    }
    
    private func setupGradient() {
        topImageGradientContainer.gradientDefaultStart = 0.5
        topImageGradientContainer.gradientDefaultEnd = 0.88
        topImageGradientContainer.gradientCutoff = 0.2
    }
}

// MARK: - Updating Model

extension VenueDetailsViewController {
    
    private func updateVenueDetails() {
        guard let venue = venue, let id = venue.id else { return }
        guard !venue.isVenueDetailsPopulated else {
            NotificationCenter.default.post(name: .venueDetailsDidUpdate, object: nil)
            return
        }
        
        queryService.fetchVenueDetails(venueId: id, receiptID: receiptID) { result in
            
            switch result {
            case let .failure(error):
                print(error)
            case let .success(venueDetails):
                self.container?.performBackgroundTask { context in
                    Venue.saveDetails(venueDetails, in: context)
                }
                NotificationCenter.default.post(name: .venueDetailsDidUpdate, object: nil)
            }
        }
    }
}

// MARK: - More Action Sheet

extension VenueDetailsViewController {
    
    private func showMoreActionSheet() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Add to List", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Check In", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Leave a Tip", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Rate", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Add a photo", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Share", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Suggest an Edit", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
}

// MARK: - CategoryTableViewControllerDelegate

extension VenueDetailsViewController: CategoryTableViewControllerDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y + scrollView.contentInset.top
//        let minHeight = view.safeAreaLayoutGuide.layoutFrame.origin.y + navBar.bounds.height + menuBar.bounds.height
        let minHeight = topViewMinHeight
        let newHeight = topViewDefaultHeight - offset
        let shownProgress = (newHeight - minHeight) / (topViewDefaultHeight - minHeight)
        
        if newHeight > minHeight {
            gradientContainerHeight?.constant = topViewDefaultHeight - offset
            topImageViewHeight.constant = topViewDefaultHeight - ((offset < 0) ? offset : 0)
            venueInfoStackView.alpha = 5 * shownProgress - 2
        } else {
            gradientContainerHeight?.constant = minHeight
        }
        topImageGradientContainer.shownProgress = shownProgress
        updateNavBar(shownProgress)
    }
}

