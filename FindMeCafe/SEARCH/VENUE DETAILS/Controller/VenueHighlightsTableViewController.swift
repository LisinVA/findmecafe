//
//  VenueHighlightsTableViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 07/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

class VenueHighlightsTableViewController: VenueDetailsPageTableViewController {
    
    // MARK: - Model
    
    var container: NSPersistentContainer? = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
    
    private var venue: Venue? {
        if let context = container?.viewContext {
            return Venue.selectedVenue(context: context)
        } else {
            return nil
        }
    }

    // TODO: rename and move to CoreData class
    // (after implementing Foursquare API fetch of the real contents)
    private var venueRating: VenueRating? { 
        return VenueRating(from: venue)
    }

    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: .venueDetailsDidUpdate, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Updating Model

extension VenueHighlightsTableViewController {
    
    @objc private func updateUI() {
        tableView.reloadData()
    }
}

// MARK: - UITableViewDelegate

extension VenueHighlightsTableViewController {
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
}

// MARK: - UITableViewDataSource

extension VenueHighlightsTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if section == 0 {
            return venueRating?.isPopulated ?? false ? 1 : 2
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellID: String
        switch indexPath.section {
        case 0: cellID = indexPath.row == 0 ? "Rating Cell" : "Rating Footer Cell"
        case 1: cellID = "Photos Cell"
        default: cellID = "Venue Features"
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        if let cell = cell as? RatingTableViewCell {
            cell.venueRating = venueRating
            return cell
        } else if let cell = cell as? RatingFooterTableViewCell {
            return cell
        } else if let cell = cell as? PhotosTableViewCell {
            return cell
        } else if let cell = cell as? FeaturesTableViewCell {
            cell.venueFeatures = VenueFeatures(
                isCheap: venue?.price?.tier ?? 0 <= 2 ? true : false,
                isTrendy: venueRating?.visitors ?? 0 > 50 ? true : false
            )
            return cell
        }
        return cell
    }
}

