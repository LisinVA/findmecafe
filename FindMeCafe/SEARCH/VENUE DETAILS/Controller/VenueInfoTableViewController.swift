//
//  VenueInfoTableViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 07/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class VenueInfoTableViewController: VenueDetailsPageTableViewController {
    
    // MARK: - Model
    
    let photos = [
        "avocado",
        "burger",
        "Rainbow_Burger"
    ]
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
        
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Bottom Cell", for: indexPath)
        
        if let cell = cell as? BottomTableViewCell {
            cell.photoImageView.image = UIImage(named: photos[indexPath.row])
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    //    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //           return UITableView.automaticDimension
    //    }
}
