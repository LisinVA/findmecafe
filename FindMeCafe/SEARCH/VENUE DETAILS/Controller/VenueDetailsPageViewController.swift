//
//  VenueDetailsPageViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 07/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class VenueDetailsPageViewController: UIPageViewController {
        
    // MARK: - Model
    
    weak var mainVC: VenueDetailsViewController?
    
    private let pageIDs = [
        "Highlights Table View Controller",
        "Info Table View Controller",
        "Photos Table View Controller",
        "Tips Table View Controller"
    ]
    
    private lazy var pages: [String: UIViewController] = {
        var pages = [String: UIViewController]()
        pageIDs.forEach { pages[$0] = getViewController(id: $0) }
        return pages
    }()
    
    private var currentIndex = 0

    private func getViewController(id: String) -> UIViewController {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id)
        
        if let categoryVC = vc as? VenueDetailsPageTableViewController {
            categoryVC.delegate = mainVC
            categoryVC.topView = mainVC?.topImageGradientContainer
            categoryVC.topViewDefaultHeight = mainVC?.topViewDefaultHeight
            categoryVC.topViewMinHeight = mainVC?.topViewMinHeight
            return categoryVC
        } else {
            return vc
        }
    }
    
    // MARK: - View Controller Lifecycle
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        dataSource = self
        if let id = pageIDs.first, let firstVC = pages[id] {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }
}

// MARK: - UIPageViewControllerDataSource

//extension VenueDetailsPageViewController: UIPageViewControllerDataSource {
//
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
//
//        guard pageIDs.count > 1 else { return nil }
//
//        let newIndex = (currentIndex - 1) >= 0 ? currentIndex - 1 : pageIDs.count - 1
//        let id = pageIDs[newIndex]
//        currentIndex = newIndex
//        return pages[id]
//    }
//
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//
//        guard pageIDs.count > 1 else { return nil }
//
//        let newIndex = (currentIndex + 1) < pageIDs.count ? currentIndex + 1 : 0
//        let id = pageIDs[newIndex]
//        currentIndex = newIndex
//        return pages[id]
//    }
//}

// MARK: - MenuBarDelegate

extension VenueDetailsPageViewController: MenuBarDelegate {
    
    func didSelectItem(at index: Int) {
        guard index >= 0, index < pageIDs.count,
            index != currentIndex,
            let newVC = pages[pageIDs[index]]
        else { return }
        
        if index > currentIndex {
            setViewControllers([newVC], direction: .forward, animated: true)
        } else {
            setViewControllers([newVC], direction: .reverse, animated: true)
        }
        currentIndex = index
    }
}
