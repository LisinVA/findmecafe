//
//  VenueTableViewCell.swift
//  FindMeCafe
//
//  Created by zweqi on 11/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit
import FoursquareAPI

// TODO: move fetching/data processing logic to ViewModel
class VenueTableViewCell: UITableViewCell {
    
    // MARK: - Model
    
    var venue: FoursquareAPI.Venue! {
        didSet {
            receiptID = venue.id
            updateUI()
        }
    }
    var venueDetails: FoursquareAPI.VenueDetails? { didSet { updateUI() } }
    var receiptID: String?
    
    // MARK: - Properties
    
    private var queryService: FoursquareAPIClient {
        return FoursquareAPIClient.shared
    }
    private var needsCategoryIcon: Bool = true
    
    // MARK: - Storyboard
    
    @IBOutlet weak var venueImageView: UIImageView!
    @IBOutlet weak var categoryIconImageView: UIImageView!
    @IBOutlet weak var venueNameLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!    // replace to Description?
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingBackgroundView: RoundedSquareView!
    
    @IBOutlet var labels: [UILabel]!
    
    // MARK: - Highlighting
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.backgroundColor = highlighted ? ColorPalette.venueSelectionColor : .white
    }
}

// MARK: - Updating UI

extension VenueTableViewCell {
    
    private func updateUI() {
        venueImageView.image = UIImage(named: "venue_preview")
        categoryIconImageView.image = nil
        needsCategoryIcon = true
        
        if let url = venue.bestPhoto?.url(size: .size100x100) {
            updateBestPhoto(url: url)
        }
        if let url = venue.categoryIcon?.url(size: .size32x32) {
            updateCategoryIcon(url: url)
        }
        updateVenueDetails()
        updateLabels()
    }
    
    private func updateCategoryIcon(url: URL) {
        guard let id = receiptID else { return }
        
        queryService.fetchVenueImage(from: url, receiptID: id) { [weak self] result in
            switch result {
            case let .failure(error):
                print(error)
            case let .success(icon):
                let currentURL = self?.venue.categoryIcon?.url(size: .size32x32)
                guard url == currentURL else { break }
                if self?.needsCategoryIcon == true {
                    self?.categoryIconImageView.image = icon
                }
            }
        }
    }
    
    private func updateBestPhoto(url: URL) {
        guard let id = receiptID else { return }
        
        queryService.fetchVenueImage(from: url, receiptID: id) { [weak self] result in
            switch result {
            case let .failure(error):
                print(error)
            case let .success(photo):
                let currentURL = self?.venue.bestPhoto?.url(size: .size100x100)
                guard url == currentURL else { break }
                self?.venueImageView.image = photo
                self?.categoryIconImageView.image = nil
                self?.needsCategoryIcon = false
            }
        }
    }
    
    private func updateVenueDetails() {
        guard let id = receiptID else { return }
        
        queryService.fetchVenueDetails(venueId: venue.id, receiptID: id) { [weak self] result in
            
            switch result {
            case let .failure(error):
                print(error)
            case let .success(venueDetails):
                self?.venueDetails = venueDetails
                self?.updateLabels()
            }
        }
    }
    
    func updateLabels() {
        venueNameLabel.text = venue.name
        reviewLabel.text = venue.tip
        categoryLabel.text = venue.category
        priceLabel.attributedText = venueDetails?.price?.attributed(style: .gray)
        distanceLabel.text = String(fromDistanceInMeters: venue.location?.distance)
        cityLabel.text = venue.location?.city ?? venue.location?.country
        ratingLabel.text = venueDetails?.rating?.value?.stringValue
        ratingBackgroundView.backgroundColor = venueDetails?.rating?.colorHex?.uicolor
        
        labels.forEach { label in
            if label.text != nil || label.attributedText != nil {
                label.isHidden = false
            } else {
                label.isHidden = true
            }
        }
        ratingLabel.superview?.isHidden = ratingLabel.isHidden
    }
}
