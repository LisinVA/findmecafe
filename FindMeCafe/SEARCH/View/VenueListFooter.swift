//
//  VenueListFooter.swift
//  FindMeCafe
//
//  Created by zweqi on 06/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class VenueListFooter: UIView {

    enum FooterType {
        case compact, medium, full
    }
    
    var footerType: FooterType = .full { didSet { updateUI() } }
    
    @IBOutlet weak var searchImageView: UIImageView!
    @IBOutlet weak var noResultsFoundLabel: UILabel!
    @IBOutlet weak var tryExpandingSearchAreaFullLabel: UILabel!
    @IBOutlet weak var tryExpandingSearchAreaMediumLabel: UILabel!
    @IBOutlet weak var addNewPlaceLabel: UILabel!
    @IBOutlet weak var expandSearchAreaButton: RoundedButton!
    
    private func updateUI() {
        searchImageView.isHidden = !(footerType == .full)
        noResultsFoundLabel.isHidden = !(footerType == .full)
        tryExpandingSearchAreaFullLabel.isHidden = !(footerType == .full)
        tryExpandingSearchAreaMediumLabel.isHidden = !(footerType == .medium)
        expandSearchAreaButton.isHidden = (footerType == .compact)
    }
}
