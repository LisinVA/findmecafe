//
//  CategoryView.swift
//  FindMeCafe
//
//  Created by zweqi on 30/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//


// Inside the top container CategoryViews arranged like this :
//
//  ̲ -----------
//  ̲| x | x | x |
//  |--- --- ---|
//  | x | x | x |
//   -----------
//

import UIKit

@IBDesignable
class CategoryView: UIView {

    private enum Position: String {
        case topLeft, topCenter, topRight
        case bottomLeft, bottomCenter, bottomRight
        case unknown
    }
    private enum BorderType {
        case upper, lower, left, right
    }
    
    // MARK: - Properties
    
    private var borders = [UIView]()
    
//    @IBInspectable private var regularContentInset: CGFloat = 8 {
//        didSet{ updateInsetConstraints() }
//    }
//    @IBInspectable private var compactContentInset: CGFloat = 0 {
//        didSet{ updateInsetConstraints() }
//    }
//    private var regularConstraints = [NSLayoutConstraint]()
//    private var compactConstraints = [NSLayoutConstraint]()
    
    // MARK: - Outlets
    
//    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    // MARK: - Drawing
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        borders.forEach { $0.removeFromSuperview() }
        borders.removeAll()
        
        switch positionInSuperview {
        case .topLeft: addBorders(type: [.lower, .right])
        case .topCenter: addBorders(type: [.left, .lower, .right])
        case .topRight: addBorders(type: [.left, .lower])
        case .bottomLeft: addBorders(type: [.upper, .right])
        case .bottomCenter: addBorders(type: [.left, .upper, .right])
        case .bottomRight: addBorders(type: [.left, .upper])
        case .unknown: break
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentMode = .redraw
//        updateInsetConstraints()
    }
}

// MARK: - Position in Container

extension CategoryView {

    private var positionInSuperview: Position {
        guard let container = superview?.superview else { return .unknown }
        let x = container.bounds.width / 2
        let y = container.bounds.height / 2
        let center = convert(self.center, to: container)
        
        switch center {
        case let center where (center.x < x && center.y < y): return .topLeft
        case let center where (center.x == x && center.y < y): return .topCenter
        case let center where (center.x > x && center.y < y): return .topRight
        case let center where (center.x < x && center.y > y): return .bottomLeft
        case let center where (center.x == x && center.y > y): return .bottomCenter
        case let center where (center.x > x && center.y > y): return .bottomRight
        default: return .unknown
        }
    }
}

// MARK: - Drawing Inner Borders

extension CategoryView {

    private func addBorders(type: [BorderType]) {
        type.forEach { type in
            
            let border = UIView()
            border.backgroundColor = ColorPalette.categoryViewBorder
            let thickness: CGFloat = 0.5
            
            switch type {
            case .upper:
                border.frame = CGRect(x: 0, y: -thickness/2,
                                      width: bounds.width, height: thickness)
            case .lower:
                border.frame = CGRect(x: 0, y: bounds.height - thickness/2,
                                      width: bounds.width, height: thickness)
            case .left:
                border.frame = CGRect(x: -thickness/2, y: 0,
                                      width: thickness, height: bounds.height)
            case .right:
                border.frame = CGRect(x: bounds.width - thickness/2, y: 0,
                                      width: thickness, height: bounds.height)
            }
            addSubview(border)
            borders.append(border)
        }
    }
}

// MARK: - Touch Animation

extension CategoryView {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        animateTouch()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        let touch = touches.first!
        if !bounds.contains(touch.location(in: self)) {
            animateTouchEnding()
            return
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        animateTouchEnding()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        animateTouchEnding()
    }
    
    private func animateTouch() {
        UIView.animate(withDuration: 0.1) {
            self.categoryImageView.alpha = 0.7
            self.categoryLabel.alpha = 0.7
        }
    }
    
    private func animateTouchEnding() {
        UIView.animate(withDuration: 0.6) {
            self.categoryImageView.alpha = 1
            self.categoryLabel.alpha = 1
        }
    }
}

// MARK: - Content Inset Constraints

//extension CategoryView {
//
//    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
//        super.traitCollectionDidChange(previousTraitCollection)
//
//        if traitCollection.verticalSizeClass == .compact {
//            deactivateRegularConstraints()
//            NSLayoutConstraint.activate(compactConstraints)
//        } else {
//            deactivateCompactConstraints()
//            NSLayoutConstraint.activate(regularConstraints)
//        }
//        layoutIfNeeded()
//    }

//    private func updateInsetConstraints() {
//        guard stackView != nil else { return }
//        updateRegularConstraints()
//        updateCompactConstraints()
//    }
//
//    private func updateRegularConstraints() {
//        deactivateRegularConstraints()
//        regularConstraints.removeAll()
//
//        let inset = regularContentInset
//        regularConstraints.append(contentsOf: [
//            topAnchor.constraint(equalTo: stackView.topAnchor, constant: -inset),
//            bottomAnchor.constraint(equalTo: stackView.bottomAnchor, constant: inset),
//            leftAnchor.constraint(equalTo: stackView.leftAnchor, constant: -inset),
//            rightAnchor.constraint(equalTo: stackView.rightAnchor, constant: inset)
//        ])
//
//        if traitCollection.verticalSizeClass != .compact {
//            NSLayoutConstraint.activate(regularConstraints)
//            layoutIfNeeded()
//        }
//    }
//
//    private func updateCompactConstraints() {
//        deactivateCompactConstraints()
//        compactConstraints.removeAll()
//
//        let inset = compactContentInset
//        compactConstraints.append(contentsOf: [
//            topAnchor.constraint(equalTo: stackView.topAnchor, constant: -inset),
//            bottomAnchor.constraint(equalTo: stackView.bottomAnchor, constant: inset),
//            leftAnchor.constraint(equalTo: stackView.leftAnchor, constant: -inset),
//            rightAnchor.constraint(equalTo: stackView.rightAnchor, constant: inset)
//        ])
//
//        if traitCollection.verticalSizeClass == .compact {
//            NSLayoutConstraint.activate(compactConstraints)
//            layoutIfNeeded()
//        }
//    }
//
//    private func deactivateRegularConstraints() {
//        if regularConstraints.count > 0 && regularConstraints[0].isActive {
//            NSLayoutConstraint.deactivate(regularConstraints)
//        }
//    }
//
//    private func deactivateCompactConstraints() {
//        if compactConstraints.count > 0 && compactConstraints[0].isActive {
//            NSLayoutConstraint.deactivate(compactConstraints)
//        }
//    }
//}
