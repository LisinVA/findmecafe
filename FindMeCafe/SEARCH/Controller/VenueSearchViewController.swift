//
//  VenueSearchViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 11/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit
import CoreLocation
import FoursquareAPI

class VenueSearchViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var backgroundImageView: GradientImageView!
    @IBOutlet weak var startLogo: UIImageView!
    @IBOutlet weak var mainLogo: UIView!
    @IBOutlet weak var searchPhraseView: UIView!
    @IBOutlet weak var searchPhraseLabel: UILabel!
    @IBOutlet weak var categoryBackgroundView: GradientBordersView!
    @IBOutlet weak var categoryStackView: UIStackView!
    @IBOutlet var categoryViews: [CategoryView]!
    
    // MARK: - Properties
    
    private var startingAnimationHasBeenShown = false
    private var locationManager: LocationManager {
        return LocationManager.shared
    }
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: find another way of getting similar results
        sleep(1)    // added to show starting logo a bit longer,
                    // to delay tab bar appearance,
                    // and to buy the time for location manager to load up
        backgroundImageView.applyMotionEffect(magnitude: 10.0)
        addTapGestureToCategoryViews()
        addTopImageGradient()
        prepareStartingAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationManager.delegate = self
        locationManager.locationQuality = .high

        if !startingAnimationHasBeenShown {
            startingAnimationHasBeenShown = true
            performStartingAnimation()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        locationManager.locationQuality = .low
    }
}

// MARK: - Navigation

extension VenueSearchViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Show Search Results" {
            guard let venuesListVC = segue.destination as? VenueListCoreDataViewController,
                let intent = sender as? SearchIntent else { return }
            venuesListVC.queryIntent = intent
//            URLCache.shared.removeAllCachedResponses()
        }
    }
}

// MARK: - Status Bar

extension VenueSearchViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


// MARK: - Top Image

extension VenueSearchViewController {
    
    private func addTopImageGradient() {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor,  UIColor.clear.cgColor]
        gradient.locations = [0.6, 0.95, 1]
        backgroundImageView.gradient = gradient
    }
}

// MARK: - Animation

extension VenueSearchViewController {
    
    private func prepareStartingAnimation() {
        searchPhraseView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        searchPhraseView?.isHidden = false
        
        categoryStackView.transform = CGAffineTransform(translationX: 0, y: 80)
        categoryStackView.alpha = 0
        
        mainLogo.transform = CGAffineTransform(translationX: 0, y: -50)
    }
    
    private func performStartingAnimation() {
        animateSearchPhrase() { finished in
            self.animateCategoryButtons() { finished in
                self.animateBackgroundImage()
                self.animateLogo()
            }
        }
    }
    
    private func animateSearchPhrase(completion: ((_ finished: Bool)->())? = nil) {
        UIView.animate(
            withDuration: 0.4,
            delay: 0,
            options: .curveEaseOut,
            animations: { self.searchPhraseView.transform = .identity },
            completion: { finished in completion?(true) }
        )
    }
    
    private func animateCategoryButtons(completion: ((_ finished: Bool)->())? = nil) {
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                self.categoryStackView.alpha = 1
                self.categoryStackView.transform = .identity
            },
            completion: { finished in completion?(true) }
        )
    }
    
    private func animateBackgroundImage() {
        UIView.animate(withDuration: 0.2) {
            self.backgroundImageView.alpha = 1
        }
    }
    
    private func animateLogo() {
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                self.startLogo.alpha = 0
                self.mainLogo.alpha = 1
                self.mainLogo.transform = .identity
            }
        )
    }
}

// MARK: - Gestures

extension VenueSearchViewController {
    
    private func addTapGestureToCategoryViews() {
        categoryViews.forEach { view in
            let tap = UITapGestureRecognizer(
                target: self,
                action: #selector(viewWithCategoryTapped(_:))
            )
            view.addGestureRecognizer(tap)
        }
    }
    
    @objc private func viewWithCategoryTapped(_ recognizer: UITapGestureRecognizer) {
        if let id = recognizer.view?.accessibilityIdentifier,
            let intent = SearchIntent(rawValue: id) {
            performSegue(withIdentifier: "Show Search Results", sender: intent)
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension VenueSearchViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard let manager = manager as? LocationManager else { return }

        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            manager.startUpdatingLocation()
        case .restricted, .denied:
            Alert.showBasic(title: "Authorization Error", message: "User has not authorized access to location information.")
        case .notDetermined:
            break
        @unknown default: break
        }
    }
}

