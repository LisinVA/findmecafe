//
//  VenueListCoreDataViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 13/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

// this subclass is created only to update the Database

class VenueListCoreDataViewController: VenueListViewController {

    // leave the user a choice of a database
    var container: NSPersistentContainer? = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer

    override func insertVenues(_ newVenues: [FoursquareAPI.Venue]) {
        super.insertVenues(newVenues)
        updateDatabase(with: newVenues)
    }
    
    override func hideVenue(_ venue: FoursquareAPI.Venue, at indexPath: IndexPath) {
        super.hideVenue(venue, at: indexPath)
        updateDatabase(venueToHide: venue)
    }
    
    // a debugging utility which resets venues, hidden by the user
    override func unhideVenues() {
        guard let context = container?.viewContext else {
            putInFinishedLoadingState()
            return
        }
        
        context.perform { [weak self] in
            let request: NSFetchRequest<Venue> = Venue.fetchRequest()
            request.predicate = NSPredicate(format: "isHidden = true")
            
            (try? context.fetch(request))?.forEach { $0.isHidden = false }
            
            try? context.save()
            self?.putInFinishedLoadingState()
        }
    }
    
    override func removeHiddenVenues() {
        guard let context = container?.viewContext else {
            putInFinishedLoadingState()
            return
        }
        
        context.perform { [weak self] in
            let request: NSFetchRequest<Venue> = Venue.fetchRequest()
            request.predicate = NSPredicate(format: "isHidden = true")
            
            guard let hiddenVenues = try? context.fetch(request) else { return }
            hiddenVenues.forEach { hiddenVenue in
                self?.venues = self?.venues.filter { $0.id != hiddenVenue.id } ?? []
            }
            self?.putInFinishedLoadingState()
        }
    }
    
    private func updateDatabase(with venues: [FoursquareAPI.Venue]) {
        container?.performBackgroundTask { context in
            for venue in venues {
                _ = try? Venue.findOrCreateVenue(matching: venue, in: context)
            }
            do {
                let _ = try context.save()
            } catch {
                print(error)
            }
        }
    }
    
    private func updateDatabase(with venueDetails: FoursquareAPI.VenueDetails) {
        container?.performBackgroundTask { context in            
            Venue.saveDetails(venueDetails, in: context)
        }
    }
    
    private func updateDatabase(venueToHide: FoursquareAPI.Venue) {
        container?.performBackgroundTask { context in
            let request: NSFetchRequest<Venue> = Venue.fetchRequest()
            request.predicate = NSPredicate(format: "id = %@", venueToHide.id)

            if let venueToHide = (try? context.fetch(request))?.first {
                venueToHide.isHidden = true
                try? context.save()
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "Show Venue Details" {
            guard let venueDetailsVC = segue.destination as? VenueDetailsViewController,
                let venueCell = sender as? VenueTableViewCell else { return }
            
            if let selectedVenueDetails = venueCell.venueDetails {
                updateDatabase(with: selectedVenueDetails)
            }
            venueDetailsVC.container = container
            Venue.selectedVenueID = venueCell.venue.id
        }
    }
}
