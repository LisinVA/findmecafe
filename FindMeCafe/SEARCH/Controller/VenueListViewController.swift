//
//  VenueListViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 15/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit
import CoreLocation
import FoursquareAPI

class VenueListViewController: UIViewController {
    
    // MARK: - Model
    
    var venues: [FoursquareAPI.Venue] = []
    var hiddenVenuesIDs: [String] = []
    private var receiptID = "Venue List"
    
    // MARK: - Outlets
    
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var navBar: BaseNavigationBar!
    @IBAction func pop(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingOverlay: UIView!
    
    // MARK: - Properties
    
    private var circularRefreshControl: CircularRefreshControl!
    private var isLoadingData = false
    var numberOfCells: Int {
        return venues.count
    }
    private var visibleSpace: CGFloat {
        let insets = tableView.contentInset.top + tableView.contentInset.bottom
        let tabBarHeight = tabBarController?.tabBar.bounds.height ?? 0
        return tableView.bounds.height - insets - tabBarHeight
    }
    private var visibleContent: CGFloat {
        return tableView.visibleCells.reduce(0) { $0 + $1.bounds.height }
    }
    private var lastContentOffset: CGPoint = .zero
    
    // MARK: - Services
    
    private var locationManager: LocationManager {
        return LocationManager.shared
    }
    var lastLocation: CLLocation? = nil
    var lastLocationString: String? {
        guard let coordinates2D = self.lastLocation?.coordinate else { return nil }
        
        let userLocation = "\(coordinates2D.latitude),\(coordinates2D.longitude)"
        return userLocation
    }
    
    private var queryService: FoursquareAPI.FoursquareAPIClient {
        return FoursquareAPIClient.shared
    }
    var queryIntent: SearchIntent = .food
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastLocation = locationManager.location
        setupNavBar()
        setupSearchBar()
        setupGestures()
        setupTableView()
        setupRefreshControl()
        updateModel()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        updateSearchBarSize()
        navBar.layoutIfNeeded()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        updateSearchBarSize()
        navBar.layoutIfNeeded()         // <-- looks disgusting
        tableView.layoutIfNeeded()      // <-- looks disgusting
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        locationManager.delegate = self
        locationManager.locationQuality = .high
        tableView.indexPathsForSelectedRows?.forEach { tableView.deselectRow(at: $0, animated: true) }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        locationManager.locationQuality = .low
        restoreTabBar()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let delegate = locationManager.delegate as? VenueListViewController,
            delegate == self {
            locationManager.delegate = nil
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in
            self.updateSearchBarSize()
            self.updateTableFooter()
        })
    }
    
    // MARK: - Updating Model (Core Data)
    // these methods will be overriden by the subclass to manage the database
    
    func insertVenues(_ newVenues: [FoursquareAPI.Venue]) {
        venues = newVenues
    }

    func hideVenue(_ venue: FoursquareAPI.Venue, at indexPath: IndexPath) {
        venues.remove(at: indexPath.row)
        
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
    func unhideVenues() {
        putInFinishedLoadingState() // TODO: find another way to run this method
    }
    
    func removeHiddenVenues() {
        putInFinishedLoadingState() // TODO: find another way to run this method
    }
}

// MARK: - Navigation

extension VenueListViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Show Venue Details" {
            guard let venueDetailsVC = segue.destination as? VenueDetailsViewController,
                let venueCell = sender as? VenueTableViewCell else { return }
            
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            
            venueDetailsVC.venuePreviewImage = venueCell.venueImageView.image
        }
    }
}

// MARK: - Status Bar

extension VenueListViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return navBar?.tintColor.isDark == true ? .default : .lightContent
    }
}

// MARK: - Navigation Bar

extension VenueListViewController {
    
    private func setupNavBar() {
        updateNavBarColors()
        setNeedsStatusBarAppearanceUpdate()
        navBar.delegate = self
    }
        
    private func updateNavBarColors() {
        let bgColor = ColorPalette.venueListNavBar
        let textColor: UIColor = bgColor.isDark ? .white : .black
        navBar.setBackgroundColor(bgColor)
        navBar.tintColor = textColor
    }
}

// MARK: - UINavigationBarDelegate

extension VenueListViewController: UINavigationBarDelegate {
    
    public func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

// MARK: - Search Bar
extension VenueListViewController {
    
    private func setupSearchBar() {
        updateSearchBarTitle(searchIntent: queryIntent.stringValue)
        searchBar.leftView = UIImageView(image: UIImage(named: "search_icon"))
        searchBar.leftViewMode = .always
        searchBar.delegate = self
    }
    
    private func updateSearchBarSize() {
        searchBar.bounds.size.width = navBar.bounds.width
    }
    
    private func updateSearchBarTitle(searchIntent intent: String) {
        let suffix = " near me"
        let attributedText = NSMutableAttributedString(
            string: intent + suffix,
            fontSize: 15,
            color: searchBar.tintColor
        )
        attributedText.addAttributes(
            [NSAttributedString.Key.foregroundColor: UIColor.lightGray],
            range: NSMakeRange(intent.count, suffix.count)
        )
        searchBar.attributedText = attributedText
    }
}

// MARK: - UITextFieldDelegate

extension VenueListViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = queryIntent.stringValue
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, text.count > 0 {
            updateSearchBarTitle(searchIntent: text)
        } else {
            updateSearchBarTitle(searchIntent: queryIntent.stringValue)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Tab Bar

extension VenueListViewController {
    
    private func restoreTabBar() {
        guard let tabBar = tabBarController?.tabBar else { return }
        
        let originY = view.bounds.height - tabBar.bounds.height
        UIView.animate(withDuration: 0.5) {
            tabBar.frame.origin.y = originY
        }
    }
    
    private func updateTabBar(_ scrollView: UIScrollView) {
        guard let tabBar = tabBarController?.tabBar else { return }
        guard visibleContent >= visibleSpace else { return }
        guard !scrollView.isBouncing else { return }
        
        let offset = scrollView.contentOffset.y - lastContentOffset.y
        lastContentOffset = scrollView.contentOffset
        
        let currentY = tabBar.frame.origin.y
        let minY = view.bounds.height - tabBar.bounds.height
        let maxY = view.bounds.height
        let newY = currentY + offset
        
        switch newY {
        case ...minY: tabBar.frame.origin.y = minY
        case maxY...: tabBar.frame.origin.y = maxY
        default: tabBar.frame.origin.y = newY
        }
    }
}

// MARK: - Table View Setup

extension VenueListViewController {

    private func setupTableView() {
        setupRefreshControl()
        updateTableFooter()
    }
    
    private func updateTableFooter() {
        guard let footer = tableView.tableFooterView as? VenueListFooter else { return }
        
        let emptySpace = visibleSpace - visibleContent
        
        switch visibleContent {
        case 0:
            footer.footerType = .full
            footer.bounds.size.height = emptySpace
        case visibleSpace...:
            footer.footerType = .compact
            let minHeight = footer.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            footer.bounds.size.height = minHeight
        default:
            footer.footerType = .medium
            let minHeight = footer.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            let newHeight = emptySpace > minHeight ? emptySpace : minHeight
            footer.bounds.size.height = newHeight
        }
        tableView.updateFooterViewFrame()
    }
}

// MARK: - Refresh Control

extension VenueListViewController {
    
    private func setupRefreshControl() {
//        tableView.refreshControl = nil
        circularRefreshControl = CircularRefreshControl()
        tableView.addSubview(circularRefreshControl)
    }
    
    private func refreshControlDidScroll(_ scrollView: UIScrollView) {
        let pullDistance = CGFloat.maximum(0.0, -tableView.contentOffset.y)
        circularRefreshControl.frame = CGRect(x: 0, y: -pullDistance, width: scrollView.bounds.width, height: pullDistance)
    }
    
    private func hideRefreshControl() {
        let offset = tableView.contentInset.top
        tableView.contentInset.top = 0
        tableView.contentOffset.y = -offset
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
}

// MARK: - Updating Model

extension VenueListViewController {
    
    @objc private func updateModel() {
        guard !isLoadingData else { return }
        guard locationManager.location != nil else {
            Alert.showBasic(title: "Location Service Error", message: "Could not get current location.")
            return
        }
        putInLoadingState()
        performDataFetch()
    }
    
    private func putInLoadingState() {
        isLoadingData = true
        if circularRefreshControl.isRefreshing {
            lastLocation = locationManager.location
        } else {
            loadingOverlay.isHidden = false
        }
    }
    
    private func performDataFetch() {
        queryService.fetchVenues(ll: lastLocationString!, limit: "3", intent: queryIntent, receiptID: receiptID) { [weak self] result in
            
            switch result {
            case let .failure(error):
                print(error)
                self?.putInFinishedLoadingState()
            case let .success(venues):
                self?.insertVenues(venues)
//                self?.unhideVenues()  // used for debugging
                self?.removeHiddenVenues()
            }
        }
    }
    
    func putInFinishedLoadingState() {
        isLoadingData = false
        tableView.reloadData()
        if circularRefreshControl.isRefreshing {
            circularRefreshControl.endRefreshing()
            hideRefreshControl()
        }
        updateTableFooter()
        loadingOverlay.isHidden = true
    }
}

// MARK: - UITableViewDataSource

extension VenueListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Found Venue Cell", for: indexPath)
        
        if let cell =  cell as? VenueTableViewCell {
            if let id = cell.receiptID {
                queryService.cancelRunningRequests(for: id)
            }
            cell.venue = venues[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            if let cell = tableView.cellForRow(at: indexPath) as? VenueTableViewCell {
                hideVenue(cell.venue, at: indexPath)
            }
        }
    }
}

// MARK: - UIScrollViewDelegate

extension VenueListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        refreshControlDidScroll(scrollView)
        updateTabBar(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let pullDistance = CGFloat.maximum(0.0, -tableView.contentOffset.y)
        if pullDistance >= circularRefreshControl.threshold,
            !circularRefreshControl.isRefreshing {
            scrollView.contentInset.top = circularRefreshControl.heightWhenRefreshing
            circularRefreshControl.beginRefreshing()
            updateModel()
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension VenueListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let manager = manager as? LocationManager else { return }
        guard let location = locations.last,
            location.horizontalAccuracy <= manager.desiredAccuracy else { return }
        
        if lastLocation == nil || location.distance(from: lastLocation!) > manager.distanceFilter {
            lastLocation = location
            updateModel()
        }
    }
}

// MARK: - Gestures

extension VenueListViewController {
    
    private func setupGestures() {
        setupTapGesture()
        setupLongPressGesture()
    }
    
    private func setupTapGesture() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func handleTap() {
        view.endEditing(true)
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        tableView.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint),
                let cell = tableView.cellForRow(at: indexPath) as? VenueTableViewCell {
                showVenueActionSheet(for: cell)
            }
        }
    }
}

// MARK: - Venue Action Sheet

extension VenueListViewController {
    
    private func showVenueActionSheet(for cell: VenueTableViewCell) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Add to List...", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Share", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Report...", style: .destructive, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true)
    }
}
