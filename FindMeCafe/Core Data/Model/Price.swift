//
//  Price.swift
//  FindMeCafe
//
//  Created by zweqi on 11/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

public class Price: NSManagedObject {

    class func findOrCreatePrice(matching price: FoursquareAPI.Price, in context: NSManagedObjectContext) throws -> Price {
        
        let request: NSFetchRequest<Price> = Price.fetchRequest()
        request.predicate = NSPredicate(format: "currency = %@ && tier = %@", price.currency ?? "", price.tier ?? 0)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "Price.findOrCreatePrice -- database inconsistency!")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let newPrice = Price(context: context)
        newPrice.currency = price.currency
        newPrice.tier = Int16(price.tier ?? 0)
        return newPrice
    }
}

extension Price {
    
    func attributed(style: FoursquareAPI.Price.Style) -> NSAttributedString? {
        let price = FoursquareAPI.Price(tier: Int(tier), currency: currency)
        return price.attributed(style: style)
    }
}
