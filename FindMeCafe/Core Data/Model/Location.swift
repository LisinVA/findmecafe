//
//  Location.swift
//  FindMeCafe
//
//  Created by zweqi on 11/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

public class Location: NSManagedObject {

    class func findOrCreateLocation(matching location: FoursquareAPI.Location, in context: NSManagedObjectContext) throws -> Location {
        
        let request: NSFetchRequest<Location> = Location.fetchRequest()
        request.predicate = NSPredicate(format: "address = %@ && city = %@ && country = %@", location.address ?? "", location.city ?? "", location.country ?? "")
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "Location.findOrCreateLocation -- database inconsistency!")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let newLocation = Location(context: context)
        newLocation.address = location.address
        newLocation.city = location.city
        newLocation.country = location.country
        return newLocation
    }
}
