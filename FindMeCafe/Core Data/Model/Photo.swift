//
//  Photo.swift
//  FindMeCafe
//
//  Created by zweqi on 11/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

public class Photo: NSManagedObject {

    class func findOrCreatePhoto(matching photoURL: FoursquareAPI.PhotoURL, in context: NSManagedObjectContext) throws -> Photo {
        
        let request: NSFetchRequest<Photo> = Photo.fetchRequest()
        request.predicate = NSPredicate(format: "url.prefix = %@ && url.suffix = %@ ", photoURL.prefix ?? "", photoURL.suffix ?? "")
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "Photo.findOrCreatePhoto -- database inconsistency!")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let newPhoto = Photo(context: context)
        newPhoto.url = PhotoURL(context: context)
        newPhoto.url?.prefix = photoURL.prefix
        newPhoto.url?.suffix = photoURL.suffix
        return newPhoto
    }
}

extension Photo {
    
    func url(size: FoursquareAPI.PhotoSize) -> URL? {
        let photoURL = FoursquareAPI.PhotoURL(prefix: url?.prefix, suffix: url?.suffix)
        return photoURL.url(size: size)
    }
}
