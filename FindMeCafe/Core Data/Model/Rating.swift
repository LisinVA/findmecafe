//
//  Rating.swift
//  FindMeCafe
//
//  Created by zweqi on 11/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

public class Rating: NSManagedObject {

    class func findOrCreateRating(matching rating: FoursquareAPI.VenueDetails.Rating, in context: NSManagedObjectContext) throws -> Rating {
        
        let request: NSFetchRequest<Rating> = Rating.fetchRequest()
        request.predicate = NSPredicate(format: "colorHex = %@ && value = %@", rating.colorHex ?? "", rating.value ?? 0)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "Rating.findOrCreateRating -- database inconsistency!")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let newRating = Rating(context: context)
        newRating.colorHex = rating.colorHex
        newRating.value = rating.value ?? 0
        return newRating
    }
}
