//
//  Venue.swift
//  FindMeCafe
//
//  Created by zweqi on 11/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit
import CoreData
import FoursquareAPI

public class Venue: NSManagedObject {
    
    // keep track of selected venue for exploring Venue Details
    static var selectedVenueID: String?
    
    class func selectedVenue(context: NSManagedObjectContext) -> Venue? {
        guard let id = selectedVenueID else { return nil }
        
        let request: NSFetchRequest<Venue> = Venue.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", id)
        return (try? context.fetch(request))?.first
    }

    class func findOrCreateVenue(matching venue: FoursquareAPI.Venue, in context: NSManagedObjectContext) throws -> Venue {
        
        let request: NSFetchRequest<Venue> = Venue.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", venue.id)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "Venue.findOrCreateVenue -- database inconsistency!")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        // create new venue since there is none in DB
        let newVenue = Venue(context: context)
        newVenue.id = venue.id
        newVenue.name = venue.name
        newVenue.category = venue.category
        newVenue.featuredTipText = venue.tip
        newVenue.distance = Int16(venue.location?.distance ?? 0)
        newVenue.categoryIconURL = IconURL(context: context)
        newVenue.categoryIconURL?.prefix = venue.categoryIcon?.suffix
        newVenue.categoryIconURL?.prefix = venue.categoryIcon?.suffix
        
        if let bestPhoto = venue.bestPhoto {
            newVenue.featuredPhoto = try? Photo.findOrCreatePhoto(matching: bestPhoto, in: context)
        }
        if let newLocation = venue.location {
            newVenue.location = try? Location.findOrCreateLocation(matching: newLocation, in: context)
        }
        return newVenue
    }
    
    class func saveDetails(_ venueDetails: FoursquareAPI.VenueDetails, in context: NSManagedObjectContext) {
        
        guard let price = venueDetails.price,
            let rating = venueDetails.rating,
            let likes = venueDetails.likes
        else { return }

        let request: NSFetchRequest<Venue> = Venue.fetchRequest()
        request.predicate = NSPredicate(format: "id = %@", venueDetails.venueID)
        
        if let venue = (try? context.fetch(request))?.first {
            venue.price = Price(context: context)
            venue.price?.currency = price.currency
            venue.price?.tier = Int16(price.tier ?? 0)
            venue.rating = Rating(context: context)
            venue.rating?.colorHex = rating.colorHex
            venue.rating?.value = rating.value ?? 0
            venue.votesCount = Int16(venueDetails.rating?.votesCount ?? 0)
            venue.likes = Int16(likes)
            
            venue.isVenueDetailsPopulated = true
            try? context.save()
        }
    }
}
