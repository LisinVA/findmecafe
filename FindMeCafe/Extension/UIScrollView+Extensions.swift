//
//  UIScrollView+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 22/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    var isBouncing: Bool {
        return isBouncingTop || isBouncingBottom || isBouncingLeft || isBouncingRight
    }
    var isBouncingTop: Bool {
        return contentOffset.y < -contentInset.top
    }
    var isBouncingBottom: Bool {
        let contentFillsScrollEdges = contentSize.height + contentInset.top + contentInset.bottom >= bounds.height
        return contentFillsScrollEdges && contentOffset.y > contentSize.height + contentInset.bottom - bounds.height
    }
    var isBouncingLeft: Bool {
        return contentOffset.x < -contentInset.left
    }
    var isBouncingRight: Bool {
        let contentFillsScrollEdges = contentSize.width + contentInset.left + contentInset.right >= bounds.width
        return contentFillsScrollEdges && contentOffset.x > contentSize.width + contentInset.right - bounds.width
    }
}
