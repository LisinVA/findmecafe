//
//  Notification.Name+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 03/12/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let venueDetailsDidUpdate = Notification.Name(rawValue: "venueDetailsDidUpdate")
}
