//
//  UIBezierPath+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 20/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension UIBezierPath {
    
    convenience init(lineStart start: CGPoint, lineEnd end: CGPoint) {
        self.init()
        
        move(to: start)
        addLine(to: end)
    }
    
//    class func linePath(start: CGPoint, end: CGPoint) -> UIBezierPath {
//        let path = UIBezierPath()
//        path.move(to: start)
//        path.addLine(to: end)
//        path.close()
//        return path
//    }
}
