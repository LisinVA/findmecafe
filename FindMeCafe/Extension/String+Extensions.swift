//
//  String+UIColor.swift
//  FindMeCafe
//
//  Created by zweqi on 27/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    var uicolor: UIColor? {
        guard self.count == 6 else { return nil }
        
        var rgbValue:UInt32 = 0
        Scanner(string: self).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    var wrappedInQuotes: String {
        return "\"" + self + "\""
    }
    
     init?(fromDistanceInMeters distance: Int?) {
        guard let distance = distance else { return nil }
        
        if distance >= 1000 {
            let km = round(Double(distance / 100)) / 10
            self = String("\(km) km")
        } else if distance > 0 {
            self = String("\(distance) m")
        } else {
            self = "0 m"
        }
    }
}
