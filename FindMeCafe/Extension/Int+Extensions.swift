//
//  Int+Extansions.swift
//  FindMeCafe
//
//  Created by zweqi on 26/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import Foundation

extension Int {
    
    var stringValue: String {
        return String(self)
    }
    
    var convertToDistance: String {
        if self >= 1000 {
            let km = round(Double(self / 100)) / 10
            return String("\(km) km")
        } else if self > 0 {
            return String("\(self) m")
        } else {
            return "0 m"
        }
    }
    
    init?(_ value: Int16?) {
        guard let value = value else { return nil }
        self.init()
        self = Int(value)
    }
}
