//
//  UINavigationController+Extensions.swift
//  CustomNavigationTransition
//
//  Created by zweqi on 17/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
