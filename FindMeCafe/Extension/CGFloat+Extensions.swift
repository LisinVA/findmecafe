//
//  CGFloat+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 29/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension CGFloat {
    
    var decimalString: String {
        let decimalFormatter = NumberFormatter()
        decimalFormatter.numberStyle = .decimal
        decimalFormatter.minimumFractionDigits = 1
        decimalFormatter.maximumFractionDigits = 1
        
        return decimalFormatter.string(from: self as NSNumber) ?? ""
    }
    
    var percentString: String {
        let percentFormatter = NumberFormatter()
        percentFormatter.numberStyle = .percent
        percentFormatter.multiplier = 100
        percentFormatter.positiveSuffix =  percentFormatter.positiveSuffix.trimmingCharacters(in: .whitespaces)
        percentFormatter.negativeSuffix =  percentFormatter.negativeSuffix.trimmingCharacters(in: .whitespaces)

        return percentFormatter.string(from: self as NSNumber) ?? ""
    }
}
