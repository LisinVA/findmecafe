//
//  CALayer+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 18/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension CALayer {
        
    func asImage() -> UIImage? {
        var image: UIImage?
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()

//            image = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return image
    }
}
