//
//  UIImageView+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 23/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension UIImageView {
    
    // fixing Bug in XCode
    // http://openradar.appspot.com/18448072

    override open func awakeFromNib() {
        super.awakeFromNib()
        self.tintColorDidChange()
    }
}
