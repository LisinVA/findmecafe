//
//  Double+StringValue.swift
//  FindMeCafe
//
//  Created by zweqi on 27/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import Foundation

extension Double {
    
    var stringValue: String {
        return String(self)
    }    
}
