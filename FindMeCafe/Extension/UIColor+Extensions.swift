//
//  UIColor+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 12/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension UIColor {
        
    var isDark: Bool {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let lum = 0.2126 * r + 0.7152 * g + 0.0722 * b
        
        return  lum < 0.50 ? true : false
    }
    
    var isExtraLight: Bool {
        let cutoff: CGFloat =  249/255

        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        // need to include more cases (light yellow colors, for example)
        return  (r > cutoff && g > cutoff && b > cutoff) ? true : false
    }
    
    var rgb: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return "r: \(r * 255), g: \(g * 255), b: \(b * 255)"
    }
}


