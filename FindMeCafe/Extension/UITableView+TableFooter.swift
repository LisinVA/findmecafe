//
//  UITableView+TableFooter.swift
//  FindMeCafe
//
//  Created by zweqi on 14/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension UITableView {
    
//    func setTableFooterView(_ footerView: UIView) {
//        footerView.translatesAutoresizingMaskIntoConstraints = false
//        tableFooterView = footerView
//
//        NSLayoutConstraint.activate([
//            footerView.leftAnchor.constraint(equalTo: self.leftAnchor),
//            footerView.rightAnchor.constraint(equalTo: self.rightAnchor),
//            footerView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
//        ])
//    }
    
    func updateFooterViewFrame() {
        guard let footerView = tableFooterView else { return }
        
        // Update the size of the footer based on its internal content.
        footerView.layoutIfNeeded()
        
        // Trigger table view to know that footer should be updated.
        let footer = tableFooterView
        tableFooterView = footer
    }
}


