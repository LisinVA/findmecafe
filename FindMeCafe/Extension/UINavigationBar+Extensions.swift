//
//  UINavigationBar+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 13/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func setBackgroundColor(_ color: UIColor) {
        let basicLayer = CALayer()
        basicLayer.frame = CGRect(x: 0, y: 0, width: 1, height: 1)
        basicLayer.backgroundColor = color.cgColor
        
        setBackgroundImage(basicLayer.asImage(), for: .default)
        shadowImage = basicLayer.asImage()
        isTranslucent = CIColor(color: color).alpha >= 1 ? false : true
    }
}
