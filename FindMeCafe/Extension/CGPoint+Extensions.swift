//
//  CGPoint+Extensions.swift
//  FindMeCafe
//
//  Created by zweqi on 21/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

extension CGPoint {
    func offsetBy(dx: CGFloat, dy: CGFloat) -> CGPoint {
        return CGPoint(x: x + dx, y: y + dy)
    }
}

