//
//  ColorPalette.swift
//  FindMeCafe
//
//  Created by zweqi on 02/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ColorPalette {
    
    // General
    
    static let navBarBlue = #colorLiteral(red: 0.2235294118, green: 0.3882352941, blue: 0.8823529412, alpha: 1)
    static let background = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)

    // SEARCH
    
    static let categoryViewBorder = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    static let categoryViewTint = #colorLiteral(red: 0.4352941215, green: 0.4431372583, blue: 0.4745098054, alpha: 1)
    
    static let venueListNavBar = #colorLiteral(red: 0.2063658093, green: 0.3843137255, blue: 0.8972167969, alpha: 1)
    static let venueSelectionColor = #colorLiteral(red: 0.9058823529, green: 0.9725490196, blue: 1, alpha: 1)
    
    static let menuBarLightText = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6982823202)
    static let menuBarDarkText = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6997538527)
    
    static let highlightsTextDark = #colorLiteral(red: 0.2605186105, green: 0.2605186105, blue: 0.2605186105, alpha: 1)
    static let highlightsTextLight = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
    static let highlightsGreen = #colorLiteral(red: 0.5131191611, green: 0.8316232562, blue: 0.3257772326, alpha: 1)
    static let highlightsTVGray = #colorLiteral(red: 0.8470832705, green: 0.8470832705, blue: 0.8470832705, alpha: 1)

    // LISTS
    
    static let listsTextFontBlue = #colorLiteral(red: 0.2117647059, green: 0.2784313725, blue: 0.8823529412, alpha: 1)
    static let listsTextFontGray = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
}
