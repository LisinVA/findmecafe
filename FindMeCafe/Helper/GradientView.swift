//
//  GradientView.swift
//  FindMeCafe
//
//  Created by zweqi on 05/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    lazy var gradient: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        layer.mask = gradient
        return gradient
    }()
    
    // current height / default(initial) height
    var shownProgress: CGFloat = 1 { didSet{ adjustTransparency() } }
    
    var gradientDefaultStart: NSNumber = 0 { didSet { updateStartingLocations() } }
    var gradientDefaultEnd: NSNumber = 1 { didSet { updateStartingLocations() } }
    
    /// A point (value of the 'shownProgress' from 0 to 1) at which current view will be fully transparent.
    var gradientCutoff: CGFloat = 0 {
        willSet {
            assert(newValue >= 0 && newValue <= 1, "GradientView.gradientCutoff value must be in the range: 0...1")
        }
    }
    
    private func updateStartingLocations() {
        updateWithoutAnimation() {
            gradient.locations = [gradientDefaultStart, gradientDefaultEnd]
        }
    }
    
    private func adjustTransparency() {
        let alpha: CGFloat
        let cutoff = gradientCutoff
        let gradientStart = gradientDefaultStart
        var gradientEnd = CGFloat(gradientDefaultEnd.doubleValue)
        
        switch shownProgress {
        case ..<cutoff:
            alpha = 0
        case cutoff...1:
            alpha = (shownProgress - cutoff) / (1 - cutoff)
            gradientEnd -= (1 - gradientEnd) * (1 - shownProgress)
        default:
            alpha = 1
        }
        updateWithoutAnimation() {
            gradient.colors = [UIColor.black.withAlphaComponent(alpha).cgColor, UIColor.clear.cgColor]
            gradient.locations = [gradientStart, gradientEnd as NSNumber]
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateWithoutAnimation() { gradient.frame = self.bounds }
    }
    
    private func updateWithoutAnimation(_ closure: () -> Void) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        closure()
        CATransaction.commit()
    }
}
