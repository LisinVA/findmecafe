//
//  RoundedButton.swift
//  FindMeCafe
//
//  Created by zweqi on 06/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            setNeedsDisplay()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if buttonType == .custom {
                UIView.animate(
                    withDuration: 0.1,
                    delay: 0,
                    options: [.beginFromCurrentState, .allowUserInteraction],
                    animations: { self.alpha = self.isHighlighted ? 0.5 : 1 }
                )
            }
        }
    }
}
