//
//  SingleItemPagingHorizontalFlowLayout.swift
//  FindMeCafe
//
//  Created by zweqi on 15/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class SingleItemPagingHorizontalFlowLayout: UICollectionViewFlowLayout {
    
//    // could be triggered multiple times even without device rotation
//    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
//        return true
//    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        guard let collectionView = collectionView else {
            return super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
        }
        let pageWidth = itemSize.width + minimumLineSpacing
        let approximatePage = collectionView.contentOffset.x / pageWidth
        let currentPage = (velocity.x > 0.0) ? floor(approximatePage) : ceil(approximatePage)
        let nextPage = (velocity.x > 0.0) ? ceil(approximatePage) : floor(approximatePage)
        
        let pannedLessThanAPage = abs(1 + currentPage - approximatePage) > 0.5
        let flicked = abs(velocity.x) > 0.3
        if (pannedLessThanAPage && flicked) {
            return CGPoint(x: nextPage * pageWidth, y: proposedContentOffset.y)
        } else {
            return CGPoint(x: round(approximatePage) * pageWidth, y: proposedContentOffset.y)
        }
    }
}

