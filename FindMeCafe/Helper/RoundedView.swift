//
//  RoundedView.swift
//  FindMeCafe
//
//  Created by zweqi on 06/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedView: UIView {

    // corners
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            setNeedsDisplay()
        }
    }
    
    // shadow
    @IBInspectable var shadowColor: UIColor = .clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
            setNeedsDisplay()
        }
    }
    @IBInspectable var shadowOpacity: CGFloat = 1.0 {
        didSet {
            layer.shadowOpacity = Float(shadowOpacity)
            setNeedsDisplay()
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
            setNeedsDisplay()
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize.zero {
        didSet {
            layer.shadowOffset = shadowOffset
            setNeedsDisplay()
        }
    }
}
