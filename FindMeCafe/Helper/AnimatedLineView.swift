//
//  AnimatedLineView.swift
//  FindMeCafe
//
//  Created by zweqi on 20/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class AnimatedLineView: UIView {
    
    @IBInspectable var trackStrokeColor: UIColor = ColorPalette.highlightsTextLight {
        didSet { setNeedsDisplay() }
    }
    @IBInspectable var animationStrokeColor: UIColor = ColorPalette.highlightsGreen {
        didSet { updateLayers() }
    }

    var animationDuration: CFTimeInterval = 1
    
    private var trackLayer = CAShapeLayer()
    private var animationLayer = CAShapeLayer()
    private var lineStart: CGPoint {
        return CGPoint(x: bounds.width - lineWidth/2, y: bounds.midY)
    }
    private var lineEnd: CGPoint {
        return CGPoint(x: lineWidth/2, y: bounds.midY)
    }
    var lineWidth: CGFloat {
        return bounds.height
    }
    
    // MARK: - Drawing
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayers()
    }
}

// MARK: - Animation

extension AnimatedLineView {
    
    private func configure() {
        layer.addSublayer(trackLayer)
        layer.addSublayer(animationLayer)
    }
    
    private func updateLayers() {
        updateTrackLayer()
        updateAnimationLayer()
    }

    private func updateTrackLayer() {
        let layer = trackLayer
        layer.path = UIBezierPath(lineStart: lineStart, lineEnd: lineEnd).cgPath
        layer.lineWidth = lineWidth
        layer.strokeColor = trackStrokeColor.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
    }
    
    private func updateAnimationLayer() {
        let layer = animationLayer
        layer.path = UIBezierPath(lineStart: lineEnd, lineEnd: lineStart).cgPath
        layer.lineWidth = lineWidth
        layer.strokeColor = animationStrokeColor.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.strokeEnd = 0
    }
    
    func animate(progress: CGFloat) {
        let strokeEndAnimation = CAKeyframeAnimation(keyPath: "strokeEnd")
        strokeEndAnimation.timingFunction = CAMediaTimingFunction.easeOutCubic
        strokeEndAnimation.duration = animationDuration
        strokeEndAnimation.values = [0.0, progress]
        strokeEndAnimation.keyTimes = [0.0, 1.0]
        strokeEndAnimation.fillMode = CAMediaTimingFillMode.forwards
        strokeEndAnimation.isRemovedOnCompletion = false
        
        animationLayer.add(strokeEndAnimation, forKey: "looping")
    }
}
