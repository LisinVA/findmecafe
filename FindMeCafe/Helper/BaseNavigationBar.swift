//
//  BaseNavigationBar.swift
//  FindMeCafe
//
//  Created by zweqi on 20/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class BaseNavigationBar: UINavigationBar {

    @IBInspectable var customBarTintColor: UIColor = .clear {
        didSet {
            setBackgroundColor(customBarTintColor)
            setNeedsDisplay()
        }
    }
    
    override var barPosition: UIBarPosition {
        return delegate == nil ? .topAttached : super.barPosition
    }
}
