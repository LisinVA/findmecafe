//
//  Alert.swift
//  FindMeCafe
//
//  Created by zweqi on 13/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class Alert {
    
    class func showBasic(title: String, message: String, vc: UIViewController? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        let vc = vc ?? UIApplication.topViewController()
        vc?.present(alert, animated: true)
    }
}
