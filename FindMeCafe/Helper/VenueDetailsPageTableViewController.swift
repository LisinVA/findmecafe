//
//  VenueDetailsPageTableViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 08/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

protocol CategoryTableViewControllerDelegate: class {
    func scrollViewDidScroll(_ scrollView: UIScrollView)
}

// TODO: simplify offset logic; add autolayout support on device rotation
class VenueDetailsPageTableViewController: UITableViewController {
    
    weak var delegate: CategoryTableViewControllerDelegate?
    
    // MARK: - Properties
    
    weak var topView: UIView!
    var topViewDefaultHeight: CGFloat!
    var topViewMinHeight: CGFloat!
    
    private var needsToUpdateOffset: Bool = true
    
    private var startingOffset: CGFloat?
    private var neededOffset: CGFloat? {
        let topViewCurrentHeight = topView.bounds.height
        var neededOffset: CGFloat = topViewDefaultHeight - topViewCurrentHeight
        
        if topViewCurrentHeight == topViewMinHeight, currentOffset > neededOffset {
            neededOffset = currentOffset
        }
        return (currentOffset != neededOffset) ? neededOffset : nil
    }
    private var currentOffset: CGFloat {
        return tableView.contentInset.top + tableView.contentOffset.y
    }
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = .clear
        tableView.contentInsetAdjustmentBehavior = .never
        startingOffset = neededOffset
        tableView.contentInset.top = topViewDefaultHeight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        needsToUpdateOffset = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateContentOffset()
    }
}

// MARK: - Updating Content Offset

extension VenueDetailsPageTableViewController {
    
    private func updateContentOffset() {
        guard needsToUpdateOffset else { return }
        guard let neededOffset = neededOffset else { return }
        
        tableView.contentOffset.y = neededOffset - tableView.contentInset.top
        needsToUpdateOffset = false
        startingOffset = nil
    }
}

// MARK: - UIScrollViewDelegate

extension VenueDetailsPageTableViewController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // ignore autoscrolling after tableView.contentInset has been initialized
        if startingOffset != nil, currentOffset != startingOffset {
            return
        }
        
        if currentOffset >= 0 {
            tableView.scrollIndicatorInsets.top = scrollView.contentInset.top
        } else {
            tableView.scrollIndicatorInsets.top = -scrollView.contentOffset.y
        }
        delegate?.scrollViewDidScroll(scrollView)
    }
}
