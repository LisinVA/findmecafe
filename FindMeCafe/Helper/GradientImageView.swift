//
//  GradientView.swift
//  FindMeCafe
//
//  Created by zweqi on 05/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class GradientImageView: UIImageView {

    var gradient: CAGradientLayer? {
        didSet {
            oldValue?.removeFromSuperlayer()
            layer.mask = gradient
            setNeedsDisplay()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateWithoutAnimation() { gradient?.frame = self.bounds }
    }
    
    private func updateWithoutAnimation(_ closure: () -> ()) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        closure()
        CATransaction.commit()
    }
}
