//
//  BaseNavigationController.swift
//  FindMeCafe
//
//  Created by zweqi on 05/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    var isDuringPushAnimation = false
    weak var realDelegate: UINavigationControllerDelegate?
    override var delegate: UINavigationControllerDelegate? {
        didSet {
            if (delegate as? BaseNavigationController) != self {
                realDelegate = delegate
                delegate = nil
            } else {
                realDelegate = nil
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        interactivePopGestureRecognizer?.delegate = self
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        isDuringPushAnimation = true
        super.pushViewController(viewController, animated: animated)
    }
}

// MARK: - UINavigationControllerDelegate

extension BaseNavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        isDuringPushAnimation = false
        realDelegate?.navigationController?(navigationController, didShow: viewController, animated: animated)
    }
}

// MARK: - UIGestureRecognizerDelegate

extension BaseNavigationController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == interactivePopGestureRecognizer {
            return viewControllers.count > 1 && !isDuringPushAnimation
        } else {
            return true
        }
    }
}
