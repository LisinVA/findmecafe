//
//  RoundedSquareView.swift
//  FindMeCafe
//
//  Created by zweqi on 20/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedSquareView: UIView {
    
    // MARK: - Properties
    
    private let shapeLayer = CAShapeLayer()

    // MARK: - Drawing
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateShapeLayer()
    }
}

// MARK: - Animation

extension RoundedSquareView {
    
    private func updateShapeLayer() {
        let width = bounds.width
        let height = bounds.height
        let curveHeight = width / 50
        let cornerHeight = curveHeight * 5
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: curveHeight + cornerHeight, y: curveHeight))
        // top
        path.addQuadCurve(
            to: CGPoint(x: width - curveHeight - cornerHeight, y: curveHeight),
            controlPoint: CGPoint(x: width/2, y: -curveHeight)
        )
        // top right corner
        path.addQuadCurve(
            to: CGPoint(x: width - curveHeight, y: curveHeight + cornerHeight),
            controlPoint: CGPoint(x: width - curveHeight, y: curveHeight)
        )
        // right side
        path.addQuadCurve(
            to: CGPoint(x: width - curveHeight, y: height - curveHeight - cornerHeight),
            controlPoint: CGPoint(x: width + curveHeight, y: height/2)
        )
        // bottom right corner
        path.addQuadCurve(
            to: CGPoint(x: width - curveHeight - cornerHeight, y: height - curveHeight),
            controlPoint: CGPoint(x: width - curveHeight, y: height - curveHeight)
        )
        // bottom
        path.addQuadCurve(
            to: CGPoint(x: curveHeight + cornerHeight, y: height - curveHeight),
            controlPoint: CGPoint(x: width/2, y: height + curveHeight)
        )
        // bottom left corner
        path.addQuadCurve(
            to: CGPoint(x: curveHeight, y: height - curveHeight - cornerHeight),
            controlPoint: CGPoint(x: curveHeight, y: height - curveHeight)
        )
        // left side
        path.addQuadCurve(
            to: CGPoint(x: curveHeight, y: curveHeight + cornerHeight),
            controlPoint: CGPoint(x: -curveHeight, y: height/2)
        )
        // top left corner
        path.addQuadCurve(
            to: CGPoint(x: curveHeight + cornerHeight, y: curveHeight),
            controlPoint: CGPoint(x: curveHeight, y: curveHeight)
        )
        path.close()
        
        shapeLayer.path = path.cgPath
        layer.mask = shapeLayer
    }
}

