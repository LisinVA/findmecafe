//
//  PassThroughView.swift
//  FindMeCafe
//
//  Created by zweqi on 08/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class PassThroughView: UIView {
    
    var isReceivingTouches = false
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)

        if isReceivingTouches {
            return view
        } else {
            return view == self ? nil : view    // allow subviews receive touches
        }
    }
}
