//
//  CircularActivityIndicatorView.swift
//  FindMeCafe
//
//  Created by zweqi on 23/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class CircularActivityIndicatorView: UIView {

    enum AnimationSpeed: CGFloat {
        case slow = 3.0
        case medium = 2.0
        case fast = 1.0
    }

    // MARK: - Properties
    
    var animationSpeed = AnimationSpeed.medium
    var progress: CGFloat = 1 {
        didSet {
            progress = max(0, min(1, progress))
            updateLayers()
        }
    }
    var lineWidth: CGFloat = 3.5
    var strokeColor: UIColor = ColorPalette.listsTextFontBlue
    var fillColor: UIColor = .clear
    var circleRadius: CGFloat = 10

    private var circleLayer = CAShapeLayer()
    private var circleCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    // MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayers()
    }
}

// MARK: - Setup View

extension CircularActivityIndicatorView {
    
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.addSublayer(circleLayer)
    }
}

// MARK: - Animation

extension CircularActivityIndicatorView {
    
    private func updateLayers() {
        circleLayer.frame = layer.bounds
        
        let path = UIBezierPath()
        path.addArc(
            withCenter: circleCenter,
            radius: circleRadius/2 + circleRadius/2 * progress,
            startAngle: -.pi/2,
            endAngle: -.pi/2 + 2*(.pi) * progress,
            clockwise: true
        )
        circleLayer.path = path.cgPath
        circleLayer.opacity = Float(progress)
        circleLayer.lineWidth = lineWidth/2 + lineWidth/2 * progress
        circleLayer.strokeColor = strokeColor.cgColor
        circleLayer.fillColor = fillColor.cgColor
    }

    func startPulse() {
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 1
        scaleAnimation.toValue = 0.5
        scaleAnimation.duration = CFTimeInterval(animationSpeed.rawValue/2)
        scaleAnimation.autoreverses = true
        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        scaleAnimation.timingFunction = CAMediaTimingFunction.easeOutCubic

        circleLayer.add(scaleAnimation, forKey: "pulse")
    }
    
    func startWaves() {
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 1.0
        scaleAnimation.toValue = 1.3
        
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.fromValue = 1.0
        opacityAnimation.toValue = 0.0
        
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = CFTimeInterval(animationSpeed.rawValue)
        animationGroup.repeatCount = .greatestFiniteMagnitude
        animationGroup.timingFunction = CAMediaTimingFunction.easeOutCubic
        animationGroup.animations = [scaleAnimation, opacityAnimation]

        circleLayer.add(animationGroup, forKey: "waves")
    }
    
    func stopAnimations() {
        circleLayer.removeAllAnimations()
    }
}
