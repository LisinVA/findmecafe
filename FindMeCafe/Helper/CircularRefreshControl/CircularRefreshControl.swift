//
//  CircularRefreshControl.swift
//  FindMeCafe
//
//  Created by zweqi on 23/05/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class CircularRefreshControl: UIView {
    
    // MARK: - Properties
    
    var mainColor: UIColor = ColorPalette.listsTextFontBlue
    var animationSpeed = CircularActivityIndicatorView.AnimationSpeed.medium {
        didSet {
            draggingCircle.animationSpeed = animationSpeed
            animatingCoreCircle.animationSpeed = animationSpeed
            animatingWaveCircle.animationSpeed = animationSpeed
        }
    }
    let threshold: CGFloat = 80
    var heightWhenRefreshing: CGFloat { return threshold * 2/3 }
    var isRefreshing = false
    
    private var progress: CGFloat {
        let animationPoint = self.heightWhenRefreshing / 4
        let currentHeight = bounds.height
        if currentHeight > animationPoint {
            return (currentHeight - animationPoint) / (threshold - animationPoint)
        } else {
            return 0
        }
    }
    private lazy var draggingCircle: CircularActivityIndicatorView = {
        let view = CircularActivityIndicatorView()
        view.circleRadius = heightWhenRefreshing / 3
        view.strokeColor = mainColor
        view.fillColor = .clear
        return view
    }()
    private lazy var animatingCoreCircle: CircularActivityIndicatorView = {
        let view = CircularActivityIndicatorView()
        view.circleRadius = heightWhenRefreshing / 3
        view.strokeColor = .clear
        view.fillColor = mainColor
        return view
    }()
    private lazy var animatingWaveCircle: CircularActivityIndicatorView = {
        let view = CircularActivityIndicatorView()
        view.circleRadius = heightWhenRefreshing / 3
        view.lineWidth = 2
        view.strokeColor = mainColor
        view.fillColor = .clear
        return view
    }()

    // MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRefreshControl()
    }

    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRefreshControl()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateDraggingCircle()
    }
}

// MARK: - Setup View

extension CircularRefreshControl {
    
    private func setupRefreshControl() {
        clipsToBounds = true
        addSubview(draggingCircle)
        addSubview(animatingCoreCircle)
        addSubview(animatingWaveCircle)
        animatingCoreCircle.isHidden = true
        animatingWaveCircle.isHidden = true
        setupLayout()
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            draggingCircle.topAnchor.constraint(equalTo: self.topAnchor),
            draggingCircle.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            draggingCircle.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            draggingCircle.heightAnchor.constraint(equalToConstant: heightWhenRefreshing),
            
            animatingCoreCircle.topAnchor.constraint(equalTo: self.topAnchor),
            animatingCoreCircle.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            animatingCoreCircle.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            animatingCoreCircle.heightAnchor.constraint(equalToConstant: heightWhenRefreshing),
            
            animatingWaveCircle.topAnchor.constraint(equalTo: self.topAnchor),
            animatingWaveCircle.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            animatingWaveCircle.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            animatingWaveCircle.heightAnchor.constraint(equalToConstant: heightWhenRefreshing)
        ])
    }
}

// MARK: - Animation

extension CircularRefreshControl {
    
    private func updateDraggingCircle() {
        draggingCircle.progress = progress
    }
    
    private func startAnimating() {
        draggingCircle.isHidden = true
        animatingCoreCircle.isHidden = false
        animatingWaveCircle.isHidden = false
        animatingCoreCircle.startPulse()
        animatingWaveCircle.startWaves()
    }
    
    private func stopAnimating() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: { self.alpha = 0 }) { finished in
            self.animatingCoreCircle.stopAnimations()
            self.animatingWaveCircle.stopAnimations()
            self.animatingCoreCircle.isHidden = true
            self.animatingWaveCircle.isHidden = true
            self.draggingCircle.isHidden = false
            self.alpha = 1
        }
    }
    
    func beginRefreshing() {
        guard !isRefreshing, bounds.height >= threshold else { return }
        isRefreshing = true
        startAnimating()
    }
    
    func endRefreshing() {
        isRefreshing = false
        stopAnimating()
    }
}
