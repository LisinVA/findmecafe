//
//  TriangleCornerView.swift
//  FindMeCafe
//
//  Created by zweqi on 20/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class TriangleCornerView: UIView {
    
    enum Position: String {
        case topLeft, topRight, bottomLeft, bottomRight
    }
    
    // MARK: - Properties
    
    @IBInspectable var fillColor: UIColor = ColorPalette.navBarBlue {
        didSet { setNeedsDisplay() }
    }
    @available(*, unavailable, message: "This property is reserved for Interface Builder. Use 'position' instead.")
    @IBInspectable var positionIB: String? {
        willSet {
            if let newPosition = Position(rawValue: newValue ?? "") {
                position = newPosition
            }
        }
        didSet { updateShapeLayer() }
    }
    var position: Position = .topRight
    private let shapeLayer = CAShapeLayer()

    // MARK: - Drawing
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateShapeLayer()
    }
}

// MARK: - Animation

extension TriangleCornerView {
    
    private func configure() {
        layer.addSublayer(shapeLayer)
    }
    
    private func updateShapeLayer() {
        let point1: CGPoint
        let point2: CGPoint
        let point3: CGPoint
        
        switch position {
        case .topLeft:
            point1 = CGPoint(x: 0, y: 0)
            point2 = CGPoint(x: bounds.width, y: 0)
            point3 = CGPoint(x: 0, y: bounds.height)
        case .topRight:
            point1 = CGPoint(x: 0, y: 0)
            point2 = CGPoint(x: bounds.width, y: 0)
            point3 = CGPoint(x: bounds.width, y: bounds.height)
        case .bottomLeft:
            point1 = CGPoint(x: 0, y: 0)
            point2 = CGPoint(x: bounds.width, y: bounds.height)
            point3 = CGPoint(x: 0, y: bounds.height)
        case .bottomRight:
            point1 = CGPoint(x: bounds.width, y: 0)
            point2 = CGPoint(x: bounds.width, y: bounds.height)
            point3 = CGPoint(x: 0, y: bounds.height)
        }
        
        let path = UIBezierPath()
        path.move(to: point1)
        path.addLine(to: point2)
        path.addLine(to: point3)
        path.close()
        
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = fillColor.cgColor
    }
}
