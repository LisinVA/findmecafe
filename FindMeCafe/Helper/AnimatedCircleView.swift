//
//  AnimatedCircleView.swift
//  FindMeCafe
//
//  Created by zweqi on 20/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class AnimatedCircleView: UIView {
    
    @IBInspectable var lineWidth: CGFloat = 1 {
        didSet { setNeedsDisplay() }
    }
    @IBInspectable var trackStrokeColor: UIColor = ColorPalette.highlightsTextLight {
        didSet { setNeedsDisplay() }
    }
    @IBInspectable var animationStrokeColor: UIColor = ColorPalette.highlightsGreen {
        didSet { setNeedsDisplay() }
    }
    
    var animationDuration: CFTimeInterval = 1
    
    private var trackLayer = CAShapeLayer()
    private var animationLayer = CAShapeLayer()
    private var circleCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    private var circleRadius: CGFloat {
        return min(bounds.midX, bounds.midY) - lineWidth/2
    }
    
    // MARK: - Drawing
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayers()
    }
}

// MARK: - Animation

extension AnimatedCircleView {
    
    private func configure() {
        layer.addSublayer(trackLayer)
        layer.addSublayer(animationLayer)
    }
    
    private func updateLayers() {
        updateTrackLayer()
        updateAnimationLayer()
    }
    
    private func updateTrackLayer() {
        let layer = trackLayer
        layer.path = UIBezierPath(arcCenter: circleCenter, radius: circleRadius, startAngle: -.pi/2, endAngle: .pi/2*3, clockwise: true).cgPath
        layer.lineWidth = lineWidth / 2
        layer.strokeColor = trackStrokeColor.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
    }
    
    private func updateAnimationLayer() {
        let layer = animationLayer
        layer.path = UIBezierPath(arcCenter: circleCenter, radius: circleRadius, startAngle: -.pi/2, endAngle: .pi/2*3, clockwise: true).cgPath
        layer.lineWidth = lineWidth
        layer.strokeColor = animationStrokeColor.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.strokeEnd = 0
    }
    
    func animate(progress: CGFloat) {
        let strokeEndAnimation = CAKeyframeAnimation(keyPath: "strokeEnd")
        strokeEndAnimation.timingFunction = CAMediaTimingFunction.easeOutCubic
        strokeEndAnimation.duration = animationDuration
        strokeEndAnimation.values = [0.0, progress]
        strokeEndAnimation.keyTimes = [0.0, 1.0]
        strokeEndAnimation.fillMode = CAMediaTimingFillMode.forwards
        strokeEndAnimation.isRemovedOnCompletion = false
        
        animationLayer.add(strokeEndAnimation, forKey: "looping")
    }
}
