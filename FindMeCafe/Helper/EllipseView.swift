//
//  EllipseView.swift
//  FindMeCafe
//
//  Created by zweqi on 20/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class EllipseView: UIView {
    
    @IBInspectable var fillColor: UIColor = ColorPalette.highlightsTextLight {
        didSet { setNeedsDisplay() }
    }
    
    private var shapeLayer = CAShapeLayer()
    
    // MARK: - Drawing
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateShapeLayer()
    }
}

// MARK: - Animation

extension EllipseView {
    
    private func configure() {
        layer.addSublayer(shapeLayer)
    }
    
    private func updateShapeLayer() {
        shapeLayer.path = UIBezierPath(ovalIn: bounds).cgPath
        shapeLayer.fillColor = fillColor.cgColor
    }
}
