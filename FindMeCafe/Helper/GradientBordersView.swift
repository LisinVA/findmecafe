//
//  GradientBordersView.swift
//  FindMeCafe
//
//  Created by zweqi on 05/10/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

@IBDesignable
class GradientBordersView: UIView {
    
    private var borderWidth: CGFloat {
        return min(bounds.width, bounds.height) * 0.1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradient()
    }
    
    private func updateGradient() {
        let maskLayer = CALayer()
        maskLayer.frame = bounds
        maskLayer.shadowPath = UIBezierPath(rect: bounds.insetBy(dx: borderWidth, dy: borderWidth)).cgPath
        
        maskLayer.shadowRadius = borderWidth/5
        maskLayer.shadowOpacity = 1
        maskLayer.shadowOffset = CGSize.zero
        maskLayer.shadowColor = UIColor.black.cgColor
        
        layer.mask = maskLayer
    }
}
