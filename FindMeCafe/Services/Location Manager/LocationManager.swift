//
//  LocationManager.swift
//  FindMeCafe
//
//  Created by zweqi on 21/11/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: CLLocationManager {
    
    static let shared = LocationManager()
    
    override func startUpdatingLocation() {
        guard LocationManager.locationServicesEnabled() else {
            Alert.showBasic(title: "Location Service Error", message: "Turn On Location Services to Allow \"FindMeCafe\" to Determine Your Location.")
            return
        }
        switch LocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            super.startUpdatingLocation()
        case .notDetermined:
            requestWhenInUseAuthorization()
        case .restricted, .denied:
            Alert.showBasic(title: "Authorization Error", message: "User has not authorized access to location information.")
        @unknown default: break
        }
    }
    
    enum LocationQuality {
        case low, high
    }
    
    var locationQuality: LocationQuality = .high {
        didSet {
            switch locationQuality {
            case .low:
                distanceFilter = 1000
                desiredAccuracy = kCLLocationAccuracyHundredMeters
            case .high:
                distanceFilter = 1000
                desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            }
        }
    }
}
