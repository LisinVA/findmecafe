//
//  VenueHistoryTableViewController.swift
//  FindMeCafe
//
//  Created by zweqi on 16/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class VenueHistoryTableViewController: UITableViewController {
    
    // MARK: - Properties
    
    private var circularRefreshControl: CircularRefreshControl!
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super .viewDidLoad()
        
        setupNavBar()
        setupRefreshControl()
    }
}

// MARK: - Status Bar

extension VenueHistoryTableViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - Navigation Bar

extension VenueHistoryTableViewController {
    
    private func setupNavBar() {
        navigationController?.navigationBar.shadowImage = UIImage()
    }
}

// MARK: - Refresh Control

extension VenueHistoryTableViewController {
    
    private func setupRefreshControl() {
        refreshControl = nil
        circularRefreshControl = CircularRefreshControl()
        tableView.addSubview(circularRefreshControl)
    }
    
    private func updateRefreshControl(_ scrollView: UIScrollView) {
        let pullDistance = CGFloat.maximum(0.0, -tableView.contentOffset.y)
        circularRefreshControl.frame = CGRect(x: 0, y: -pullDistance, width: scrollView.bounds.width, height: pullDistance)
    }
    
    private func hideRefreshControl() {
        let offset = tableView.contentInset.top
        tableView.contentInset.top = 0
        tableView.contentOffset.y = -offset
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    private func refresh() {
        // animate fake fetching since there is nothing to fetch yet
        circularRefreshControl.beginRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.circularRefreshControl.endRefreshing()
            self.hideRefreshControl()
        }
    }
}

// MARK: - UITableViewDataSource

extension VenueHistoryTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellID: String = (indexPath.row == 0 ? "History Cell" : "History Footer Cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        return cell
    }
}

// MARK: - UIScrollViewDelegate

extension VenueHistoryTableViewController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateRefreshControl(scrollView)
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let pullDistance = CGFloat.maximum(0.0, -tableView.contentOffset.y)
        if pullDistance >= circularRefreshControl.threshold,
            !circularRefreshControl.isRefreshing {
            scrollView.contentInset.top = circularRefreshControl.heightWhenRefreshing
            refresh()
        }
    }
}


