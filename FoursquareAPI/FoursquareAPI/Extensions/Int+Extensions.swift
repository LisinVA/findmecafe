//
//  Int+Extensions.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation

extension Int {
    
    var stringValue: String {
        return String(self)
    }
    
    var convertToDistance: String {
        if self >= 1000 {
            let km = round(Double(self / 100)) / 10
            return String("\(km) km")
        } else if self > 0 {
            return String("\(self) m")
        } else {
            return "0 m"
        }
    }
}
