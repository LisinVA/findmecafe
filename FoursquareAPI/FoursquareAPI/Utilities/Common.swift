//
//  Common.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation

public enum Result<Value, Error> {
    case success(Value)
    case failure(Error)
}

public enum FoursquareError: Error {
    case connectionError(Error)
    case jsonParseError(Error)
    case noResultsError(Error)
}

public struct CustomError: Error, LocalizedError {
    public var errorDescription: String? {
        return NSLocalizedString(message, comment: "")
    }
    private let message: String
    
    public init(message: String) {
        self.message = message
    }
}

public enum PhotoSize: String {
    case size36x36 = "36x36"
    case size100x100 = "100x100"
    case size300x300 = "300x300"
    case size500x500 = "500x500"
}

public enum IconSize: String {
    case size32x32 = "32"
    case size44x44 = "44"
    case size64x64 = "64"
    case size88x88 = "88"
}

public enum SearchIntent: String {
    case food, breakfast, lunch, dinner, coffee, drinks, shopping
    
    public var stringValue: String {
        let intent: String
        switch self {
        case .coffee: intent = "Coffee & Tea"
        case .drinks: intent = "Nightlife"
        default: intent = self.rawValue.capitalized
        }
        return intent
    }
}

public struct Location: Codable {
    public let distance: Int?       // distance in meters
    public let address: String?
    public let city: String?
    public let country: String?
    
    public init(distance: Int?, address: String?, city: String?, country: String?) {
        self.distance = distance
        self.address = address
        self.city = city
        self.country = country
    }
}

public struct Price: Codable {
    
    public enum Style {
        case light, dark, gray
    }

    public let tier: Int?
    public let currency: String?
    
    public init(tier: Int?, currency: String?) {
        self.tier = tier
        self.currency = currency
    }
    
    public func attributed(style: Style) -> NSMutableAttributedString? {
        guard let tier = tier, let currency = currency,
            tier >= 0, tier <= 4 else { return nil }
        
        let activeText = String(repeating: currency, count: tier)
        let inactiveText = String(repeating: currency, count: 4 - tier)
        let activeTextColor: UIColor
        let inactiveTextColor: UIColor
        
        switch style {
        case .light:
            activeTextColor = ColorPalette.priceActiveLight
            inactiveTextColor = ColorPalette.priceInactiveLight
        case .dark:
            activeTextColor = ColorPalette.priceActiveDark
            inactiveTextColor = ColorPalette.priceInactiveDark
        case .gray:
            activeTextColor = ColorPalette.priceActiveGray
            inactiveTextColor = ColorPalette.priceInactiveGray
        }
        
        let attributedPrice = NSMutableAttributedString(string: activeText, fontSize: 13.0, color: activeTextColor)
        
        return attributedPrice.appended(string:
            NSMutableAttributedString(string: inactiveText, fontSize: 13.0, color: inactiveTextColor))
    }
}

public struct PhotoURL: Codable {
    public let prefix: String?
    public let suffix: String?
    
    public init(prefix: String?, suffix: String?) {
        self.prefix = prefix
        self.suffix = suffix
    }
    public func url(size: PhotoSize) -> URL? {
        guard let prefix = prefix, let suffix = suffix else { return nil }
        return URL(string: prefix + size.rawValue + suffix)
    }
}

public struct IconURL: Codable {
    public let prefix: String?
    public let suffix: String?
    
    public init(prefix: String?, suffix: String?) {
        self.prefix = prefix
        self.suffix = suffix
    }
    public func url(size: IconSize) -> URL? {
        guard let prefix = prefix, let suffix = suffix else { return nil }
        return URL(string: prefix + size.rawValue + suffix)
    }
}
