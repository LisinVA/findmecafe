//
//  ColorPalette.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

class ColorPalette {
    
    static let priceActiveLight = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let priceActiveDark = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let priceActiveGray = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    static let priceInactiveLight = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
    static let priceInactiveDark = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
    static let priceInactiveGray = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
}
