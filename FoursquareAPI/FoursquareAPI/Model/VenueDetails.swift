//
//  VenueDetails.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

public class VenueDetails: Codable {
    
    // MARK: - Model
        
    public let venueID: String         // unique venue identifier
    public let name: String            // the best known name for a venue
    
    // TODO: get rid of Optionals?
    public var price: Price?           // average price
    public var rating: Rating?
    public var venuePhotos = [PhotoURL]()
    public var likes: Int?
    
    public struct Rating: Codable {
        public let value: Double?
        public let votesCount: Int?
        public let colorHex: String?
    }
    
    // MARK: - Initializing
    
    init(from json: VenueDetailsJSON) {
        let venue = json.response.venue
        
        self.venueID = venue.id
        self.name = venue.name
        self.likes = venue.likes?.count
        self.price = Price(tier: venue.price?.tier,
                           currency: venue.price?.currency)
        self.rating = Rating(value: venue.rating,
                             votesCount: venue.ratingSignals,
                             colorHex: venue.ratingColor)
        
        venue.listed?.groups?.first?.items?.forEach { item in
            if let photo = item.photo {
                self.venuePhotos.append(photo)
            }
        }
    }
}

// MARK: - CustomStringConvertible

extension VenueDetails: CustomStringConvertible {
    
    public var description: String {
        return String("""
            venue \(name):
            venueID \(venueID),
            priceTier - \(price?.tier?.stringValue ?? ""),
            priceCurrency - \(price?.currency ?? ""),
            rating - \(rating?.value?.stringValue ?? ""),
            ratingVotes - \(rating?.votesCount?.stringValue ?? ""),
            ratingColor - \(rating?.colorHex ?? ""),
            likes - \(likes?.stringValue ?? "")
        """)
    }
}
