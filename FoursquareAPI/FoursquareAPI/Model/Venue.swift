//
//  Venue.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import UIKit

public class Venue: Codable {
    
    // MARK: - Model
    
    public let id: String          // unique identifier
    public let name: String        // the best known name for a venue
    
    // TODO: get rid of Optionals?
    public var location: Location?
    public var category: String?   // primary category for the venue
    public var tip: String?        // one random review
    public var bestPhoto: PhotoURL?
    public var categoryIcon: IconURL?
    
    // MARK: - Initializing
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

// MARK: - Equatable

extension Venue: Equatable {
    
    public static func == (lhs: Venue, rhs: Venue) -> Bool {
        return lhs.id == rhs.id
    }
}

// MARK: - CustomStringConvertible

extension Venue: CustomStringConvertible {
    
    public var description: String {
        return String("""
            venue \(name):
            id \(id),
            category - \(category ?? ""),
            address -  \(location?.address ?? ""),
            city - \(location?.city ?? ""),
            country - \(location?.country ?? ""),
            distance - \(location?.distance?.stringValue ?? ""),
            bestPhotoPrefix - \(bestPhoto?.prefix ?? ""),
            bestPhotoSuffix - \(bestPhoto?.suffix ?? ""),
            tip - \(tip ?? "")
        """)
    }
}
