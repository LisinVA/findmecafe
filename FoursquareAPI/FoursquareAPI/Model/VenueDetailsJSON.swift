//
//  VenueDetailsJSON.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation

// API: "/venues/VENUE_ID"
struct VenueDetailsJSON: Codable {
    let response: Response
    
    struct Response: Codable {
        let venue: Venue
        
        struct Venue: Codable {
            let id: String
            let name: String
            let description: String?
            let rating: Double?
            let ratingColor: String?
            let ratingSignals: Int?
            let likes: Likes?
            let price: Price?
            let listed: Listed?
            
            struct Likes: Codable {
                let count: Int?
            }
            struct Listed: Codable {
                let groups: [Group]?
                
                struct Group: Codable {
                    let items: [Item]?
                    
                    struct Item: Codable {
                        let photo: PhotoURL?
                    }
                }
            }
        }
    }
}
