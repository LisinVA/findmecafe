//
//  FoursquareAPIClient.swift
//  FoursquareAPI
//
//  Created by zweqi on 12/06/2019.
//  Copyright © 2019 zweqi. All rights reserved.
//

import Foundation
import UIKit

public class FoursquareAPIClient {
    
    public static let shared = FoursquareAPIClient()
    
    var session: URLSession {
        return URLSession.shared
    }
    lazy var cache: URLCache = {
        let cache = URLCache.shared
        cache.memoryCapacity = FoursquareAPIClient.cacheCapacity
        cache.diskCapacity = FoursquareAPIClient.cacheCapacity
        return cache
    }()
    
    struct RequestReceipt {
        let receiptID: String
        let url: URL
    }
    var requestReceipts = [RequestReceipt]()
}

// MARK: - Main API Request

extension FoursquareAPIClient {
    
    func request(path: String,
                 parameters: [String: String],
                 receiptID: String,
                 completion: @escaping (Result<Data, FoursquareError>) -> ()) {
        
        var urlComponents = URLComponents(string: FoursquareAPIClient.baseURLString + path)
        
        urlComponents?.queryItems = [
            URLQueryItem(name: "client_id", value: FoursquareAPIClient.clientId),
            URLQueryItem(name: "client_secret", value: FoursquareAPIClient.clientSecret),
            URLQueryItem(name: "v", value: FoursquareAPIClient.version),
        ]
        parameters.forEach { name, value in
            urlComponents?.queryItems?.append(
                URLQueryItem(name: name, value: value)
            )
        }
        
        guard let url = urlComponents?.url else {
            fatalError("Could not create URL with the given parameters")
        }
        
        requestReceipts.append(RequestReceipt(receiptID: receiptID, url: url))
        session.dataTask(with: url) { [weak self] data, response, error in
            self?.requestReceipts.removeAll(where: { $0.url == url })
            
            if let error = error {
                completion(.failure(.connectionError(error)))
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                completion(.success(data))
            } else if let response = response as? HTTPURLResponse {
                let error = CustomError(message: "Connection error with http status code: \(response.statusCode)")
                completion(.failure(.connectionError(error)))
            } else {
                fatalError("Unknown error while trying to retrieve data from request.")
            }
            }.resume()
    }
}

// MARK: - Handling Current Requests

extension FoursquareAPIClient {
    
    public func cancelRunningRequests(for receiptID: String) {
        let urls = requestReceipts.compactMap { receipt -> URL? in
            return receipt.receiptID == receiptID ? receipt.url : nil
        }
        session.getTasksWithCompletionHandler() { dataTasks, _, _ in
            dataTasks.forEach { task in
                if let url = task.originalRequest?.url, urls.firstIndex(of: url) != nil {
                    task.cancel()
                }
            }
        }
        requestReceipts.removeAll(where: { $0.receiptID == receiptID })
    }
}

// MARK: - Constants

extension FoursquareAPIClient {
    
    private static let baseURLString = "https://api.foursquare.com/v2"
    private static let clientId = "NSSRCQNOG4BWPLBUWKY2SKCJY2Q1YQ4BXGAIHIZLALGXP3YF"
    private static let clientSecret = "QGKCUUL1UN40BGF0IMZUIMX5X0WJYJPKLFIPI1HTMZEPQBA1"
    private static let version = "20180917"
    
    static let cacheCapacity: Int = 20 * 1024 * 1024
    static let timeout: Double = 10
    static let locale = "ru"
}
